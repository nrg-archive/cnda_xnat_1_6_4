<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">

#macro( propValueRow $name $prop )
  #if ( $!item.getProperty("$prop") )
		<tr><td class="attr_name">$name:</td><td class="attr_value">$item.getProperty("$prop")</td></tr>
	#end
#end

#macro( propValueRow $name $prop $addText)
  #if ( $!item.getProperty("$prop") )
		<tr><td class="attr_name">$name:</td><td class="attr_value">$item.getProperty("$prop") $addText</td></tr>
	#end
#end

<!-- TODO: better presentation -->
#macro( propBoolValueRow $name $prop )
	#if ($!item.getBooleanProperty("$prop"))
		<tr><td class="attr_name">$name:</td><td class="attr_value">$item.getBooleanProperty("$prop")</td></tr>
	#end
#end

<style type="text/css">
td.attr_name { <!-- in imitation of xdat.css (th) -->
	border-collapse: collapse;
	border-left-style: none;
	border-right-style: none;
	border-top-style: none;
	font-size: 11px;
	font-weight: 700;
	line-height: 13px;
	margin: 0px;
	padding-left: 4px;
	padding-right: 4px;
}

td.attr_value { <!-- in imitation of xdat.css (td) -->
	font-family: verdana,geneva,helvetica;
	font-size: 10px;
	line-height: 14px;
	padding-left: 4px;
	padding-right: 4px;
}

td.subtable {
	valign: top;
}
</style>

$page.setTitle("Vitals : $!item.getProperty('cnda:vitalsData.ID')")
$page.setLinkColor($ui.alink)
$page.setVlinkColor($ui.vlink)
<br>

#if ($data.message)
<div class="error">$data.message</div><br>
#end

#if ($data.getParameters().getString("popup"))
	#set ($popup = $data.getParameters().getString("popup") )
	#set ($popup = "false")
#end

#if (!$project)
#set($project=$item.getProperty("project"))
#end

#set($subject = $om.getSubjectData())
#set($part_id = $subject.getStringProperty("ID"))

<div class="edit_title">
Vitals:
#if($project)
	$om.getIdentifier($project)
#else
	$!item.getProperty("cnda:vitalsData.ID")
#end
</div>

#parse("/screens/workflow_alert.vm")

<table width="100%">
	<tr>
		<td valign="top"><table>
		<!-- left table -->
		#propValueRow("Date" "cnda:vitalsData/date")
		#propValueRow("Time" "cnda:vitalsData/time")
		#propValueRow("Visit ID" "cnda:vitalsData/visit_id")
		</table></td>
		<td valign="top"><table>
		<!-- middle table -->
			<tr>
				<td class="attr_name">Subject:</td>
				<td>
					<A CLASS=b HREF="$link.setAction('DisplayItemAction').addPathInfo('search_element','xnat:subjectData').addPathInfo('search_field','xnat:subjectData.ID').addPathInfo('search_value',$part_id).addPathInfo('popup','$!popup').addPathInfo('project','$!project')">
					#if($project)
						$!subject.getIdentifier($project)
					#else
						$!subject.getId()
					#end
					</A>
				</td>
			</tr>
			#if($!subject.getGenderText())
			<tr>
				<td class="attr_name">Gender:</td>
				<td class="attr_value">$subject.getGenderText()</td>
			</tr>
			#end
			#propValueRow("Age" "cnda:vitalsData/age")
		</table></td>
		<td valign="top">
		#parse("/screens/xnat_experimentData_shareDisplay.vm")
		</td>
		<td valign="top">
			#elementActionsBox($element $search_field $search_value $data.getSession().getAttribute("user") $item)
		</td>
	</tr>
</table>

#if ($!item.getStringProperty("note"))
<table>
  <tr>
		<td valign="top" class="attr_name">Notes:</td>
		<td valign="top" class="attr_value">$!item.getStringProperty("note")</td>
	</tr>
</table>
#end

<br>

<table>
		#if ($!item.getProperty("cnda:vitalsData/height"))
			<tr><td class="attr_name">Height:</td><td class="attr_value">$!item.getProperty("cnda:vitalsData/height") $!item.getProperty("cnda:vitalsData/height/units")</td></tr>
		#end
		#if ($!item.getProperty("cnda:vitalsData/weight"))
			<tr><td class="attr_name">Weight:</td><td class="attr_value">$!item.getProperty("cnda:vitalsData/weight") $!item.getProperty("cnda:vitalsData/weight/units")</td></tr>
		#end
		#propValueRow("Respirations" "cnda:vitalsData/respirations")
		#propValueRow("O<sub>2</sub> Saturation" "cnda:vitalsData/saturationO2")
		#propBoolValueRow("Problem with readings?" "cnda:vitalsData/problem")
		<tr><th></th><th>Sitting</th><th>Standing</th><tr>
		<tr>
			<td>Blood pressure</td>
			<td>$!item.getIntegerProperty("cnda:vitalsData/bloodPressure[0]/systolic")/$!item.getIntegerProperty("cnda:vitalsData/bloodPressure[0]/diastolic")</td>
			<td>$!item.getIntegerProperty("cnda:vitalsData/bloodPressure[1]/systolic")/$!item.getIntegerProperty("cnda:vitalsData/bloodPressure[1]/diastolic")</td>
		</tr>
		<tr>
			<td>Pulse</td>
			<td>$!item.getIntegerProperty("cnda:vitalsData/pulse[0]/pulse")</td>
			<td>$!item.getIntegerProperty("cnda:vitalsData/pulse[1]/pulse")</td>
		</tr>
</table>		
		

#set($xnat_experimentData_field_4_NUM_ROWS=$item.getChildItems("cnda:vitalsData/fields/field").size() - 1)
#if($xnat_experimentData_field_4_NUM_ROWS>=0)
	#foreach($xnat_experimentData_field_4_COUNTER in [0..$xnat_experimentData_field_4_NUM_ROWS])
<!-- BEGIN cnda:vitalsData/fields/field[$xnat_experimentData_field_4_COUNTER] -->
	<TABLE>
		<TR><TH align="left"><BR><font face="$ui.sansSerifFonts" size="2">cnda:vitalsData/fields/field[$xnat_experimentData_field_4_COUNTER]</font></TH></TR>
		<TR>
			<TD align="left" valign="top">
				<TABLE>
					<TR><TD>field</TD><TD>$!item.getStringProperty("cnda:vitalsData/fields/field[$xnat_experimentData_field_4_COUNTER]/field")</TD></TR>
					<TR><TD>name</TD><TD>$!item.getStringProperty("cnda:vitalsData/fields/field[$xnat_experimentData_field_4_COUNTER]/name")</TD></TR>
				</TABLE>
			</TD>
		</TR>
	</TABLE>
	#end
#end

<BR>#parse("/screens/ReportProjectSpecificFields.vm")
