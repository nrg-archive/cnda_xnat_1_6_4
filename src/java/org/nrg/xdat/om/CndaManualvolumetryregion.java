//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:38 CDT 2005
 *
 */
package org.nrg.xdat.om;
import java.text.NumberFormat;
import java.util.Hashtable;
import java.util.Iterator;

import org.nrg.xdat.om.base.BaseCndaManualvolumetryregion;
import org.nrg.xft.ItemI;
import org.nrg.xft.security.UserI;

/**
 * @author XDAT
 *
 */
public class CndaManualvolumetryregion extends BaseCndaManualvolumetryregion {

	public CndaManualvolumetryregion(ItemI item)
	{
		super(item);
	}

	public CndaManualvolumetryregion(UserI user)
	{
		super(user);
	}

	public CndaManualvolumetryregion()
	{}

	public CndaManualvolumetryregion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

	public int getVolumetryRegionSlicesCount()
	{
	    try {
            return getSlice().size();
        } catch (Exception e) {
            logger.error("",e);
            return 0;
        }
	}
	
	public Double getVolume()
	{
	    double d = 0;
	    
	    try {
            Iterator iter = getSlice().iterator();
            while (iter.hasNext())
            {
                CndaManualvolumetryregionSlice slice = (CndaManualvolumetryregionSlice)iter.next();
                Double temp = slice.getVolume();
                d += temp.doubleValue();
            }
        } catch (Exception e) {
            logger.error("",e);
        }
	    return new Double(d);
	}
	
	public String getVolumeDisplay()
	{
	    Double f = getVolume();
	    NumberFormat formatter = NumberFormat.getInstance();
		formatter.setGroupingUsed(false);
		formatter.setMaximumFractionDigits(2);
		formatter.setMinimumFractionDigits(0);
		return formatter.format(f);
	}

}
