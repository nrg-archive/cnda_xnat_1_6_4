/*
 * Copyright Washington University in St Louis 2006
 * All rights reserved
 *
 * @author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.nrg.pipeline.launchers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.PipelineFileUtils;
import org.nrg.pipeline.utils.PipelineUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParameterData.Values;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.nrg.xdat.om.XnatAbstractresource;
import org.nrg.xdat.om.XnatImageresource;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.model.XnatReconstructedimagedataI;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;

public class GenericBoldPreProcessing_PetMRLauncher extends PipelineLauncher{

    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(GenericBoldPreProcessing_PetMRLauncher.class);

    public static final String NAME = "GenericBoldPreprocessing_PetMR.xml";
    public static final String LOCATION = "build-tools";
    public static final String STDBUILDTEMPLATE = "PipelineScreen_GenericBoldPreProcessing_PetMR.vm";


    public static final String MPRAGE = "MPRAGE";
    public static final String MPRAGE_PARAM = "mprs";
    public static final String TSE = "TSE";
    public static final String TSE_PARAM = "tse";
    public static final String T1W = "T1W";
    public static final String T1W_PARAM = "t1w";
    public static final String PDT2 = "PDT2";
    public static final String PDT2_PARAM = "pdt2";
    public static final String EPI = "BOLD";
    public static final String EPI_PARAM = "fstd";


    public boolean launch(RunData data, Context context) {
        boolean rtn = false;
        try {
            ItemI data_item = TurbineUtils.GetItemBySearch(data);
            XnatPetsessiondata pet = new XnatPetsessiondata(data_item);
            XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetLauncher(data, context, pet);
            String pipelineName = data.getParameters().get("pipelinename");
            String cmdPrefix = data.getParameters().get("cmdprefix");
            xnatPipelineLauncher.setPipelineName(pipelineName);
            String buildDir = PipelineFileUtils.getBuildDir(pet.getProject(), true);
            buildDir += "stdb" ;
            xnatPipelineLauncher.setBuildDir(buildDir);
            xnatPipelineLauncher.setNeedsBuildDir(false);

            Parameters parameters = Parameters.Factory.newInstance();

            ParameterData param = parameters.addNewParameter();
            param.setName("rm_prev_folder");
            param.addNewValues().setUnique("0");

            param = parameters.addNewParameter();
            param.setName("sessionId");
            param.addNewValues().setUnique(pet.getLabel());

            param = parameters.addNewParameter();
            param.setName("project");
            param.addNewValues().setUnique(pet.getProject());


            boolean build = false;

            String target = data.getParameters().get("target");
            param = parameters.addNewParameter();
            param.setName("target");
            param.addNewValues().setUnique(target);

            param = parameters.addNewParameter();
            param.setName("TR_vol");
            param.addNewValues().setUnique(data.getParameters().get("TR_vol"));

            param = parameters.addNewParameter();
            param.setName("skip");
            param.addNewValues().setUnique(data.getParameters().get("skip"));

            if (TurbineUtils.HasPassedParameter("nx", data)) {
                String nx = data.getParameters().get("nx");
                if (nx != null && !nx.equals("")) {
                    param = parameters.addNewParameter();
                    param.setName("nx");
                    param.addNewValues().setUnique(nx);
                }
            }

            if (TurbineUtils.HasPassedParameter("ny", data)) {
                String ny = data.getParameters().get("ny");
                if (ny != null && !ny.equals("")) {
                    param = parameters.addNewParameter();
                    param.setName("ny");
                    param.addNewValues().setUnique(ny);
                }
            }

            if (TurbineUtils.HasPassedParameter("TR_slc", data)) {
                String TR_slc = data.getParameters().get("TR_slc");
                if (TR_slc != null && !TR_slc.equals("")) {
                    param = parameters.addNewParameter();
                    param.setName("TR_slc");
                    param.addNewValues().setUnique(TR_slc);
                }
            }

            if (TurbineUtils.HasPassedParameter(PipelineUtils.CROSS_DAY_REGISTER, data)) {
                String cross_day_register = data.getParameters().get(PipelineUtils.CROSS_DAY_REGISTER);
                String reconId = data.getParameters().get("recon");
                if (!cross_day_register.equals("-1")) {
                    XnatMrsessiondata crossMr = XnatMrsessiondata.getXnatMrsessiondatasById(cross_day_register, TurbineUtils.getUser(data), false);
                    if (crossMr != null) {
                        XnatReconstructedimagedataI recon = crossMr.getReconstructionByID(reconId);
                        ArrayList<XnatAbstractresource> outfiles = (ArrayList)recon.getOut_file();
                        String CODE = "ANAT_AVG_111";
                        for (int k=0; k< outfiles.size(); k++) {
                            XnatAbstractresource file = outfiles.get(k);
                            if (file instanceof XnatImageresource) {
                                XnatImageresource anat_ave_111 = (XnatImageresource)file;
                                if (CODE.equals(anat_ave_111.getContent())) {
                                    String filePath = anat_ave_111.getUri();
                                    int index = filePath.indexOf("/atlas/");
                                    if (index != -1) {
                                       param = parameters.addNewParameter();
                                       param.setName("day1_archivedir");
                                       param.addNewValues().setUnique(filePath.substring(0,index));
                                       break;
                                    }else {
                                        data.setMessage("Couldnt find path to ANAT_AVE file. Unable to set day1_archivedir parameter");
                                        return false;
                                    }
                                }
                            }
                        }

                       param = parameters.addNewParameter();
                       param.setName("day1_sessionId");
                       param.addNewValues().setUnique(crossMr.getLabel());
                    }
                }
            }


            ArrayList<String> mprs = getCheckBoxSelections(data,pet,MPRAGE);
            ArrayList<String> t2s = getCheckBoxSelections(data,pet,TSE);
            ArrayList<String> t1w = getCheckBoxSelections(data,pet,T1W);
            ArrayList<String> pdt2 = getCheckBoxSelections(data,pet,PDT2);

            ArrayList<String> bold = getCheckBoxSelections(data,pet,StdBuildLauncher.EPI);
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            String s = formatter.format(date);


            if (TurbineUtils.HasPassedParameter("build_" + MPRAGE, data)) {
                // xnatPipelineLauncher.setParameter("mprs",mprs);
                param = parameters.addNewParameter();
                param.setName("mprs");
                Values values = param.addNewValues();
                if (mprs.size() == 1) {
                    values.setUnique(mprs.get(0));
                }else {
                    for (int i = 0; i < mprs.size(); i++) {
                        values.addList(mprs.get(i));
                    }
                }
                build = true;
            }
            if (TurbineUtils.HasPassedParameter("build_" + TSE, data)) {
                param = parameters.addNewParameter();
                param.setName("tse");
                Values values = param.addNewValues();
                if (t2s.size() == 1) {
                    values.setUnique(t2s.get(0));
                }else {
                    for (int i = 0; i < t2s.size(); i++) {
                        values.addList(t2s.get(i));
                    }
                }
                build = true;
            }

            if (TurbineUtils.HasPassedParameter("build_" + T1W, data)) {
                param = parameters.addNewParameter();
                param.setName("t1w");
                Values values = param.addNewValues();
                if (t1w.size() == 1) {
                    values.setUnique(t1w.get(0));
                }else {
                    for (int i = 0; i < t1w.size(); i++) {
                        values.addList(t1w.get(i));
                    }
                }
                build = true;
            }

            if (TurbineUtils.HasPassedParameter("build_" + PDT2, data)) {
                param = parameters.addNewParameter();
                param.setName("pdt2");
                Values values = param.addNewValues();
                if (pdt2.size() == 1) {
                    values.setUnique(pdt2.get(0));
                }else {
                    for (int i = 0; i < pdt2.size(); i++) {
                        values.addList(pdt2.get(i));
                    }
                }
                build = true;
            }

            if (TurbineUtils.HasPassedParameter("build_" + EPI, data)) {
                // xnatPipelineLauncher.setParameter("fstd",bold);
                param = parameters.addNewParameter();
                param.setName("fstd");
                Values values = param.addNewValues();
                if (bold.size() == 1) {
                    values.setUnique(bold.get(0));
                }else {
                    for (int i = 0; i < bold.size(); i++) {
                        values.addList(bold.get(i));
                    }
                }

                if (bold.size() > 0) {
                    ArrayList<String> irun = getRunLabels(data,StdBuildLauncher.EPI);
                    param = parameters.addNewParameter();
                    param.setName("irun");
                     values = param.addNewValues();
                    if (irun.size() == 1) {
                        values.setUnique(irun.get(0));
                    }else {
                        for (int i = 0; i < irun.size(); i++) {
                            values.addList(irun.get(i));
                        }
                    }

                    String epidir = data.getParameters().get("epidir");
                    param = parameters.addNewParameter();
                    param.setName("epidir");
                    param.addNewValues().setUnique(epidir);

                    String epi2atl = data.getParameters().get("epi2atl");
                    param = parameters.addNewParameter();
                    param.setName("epi2atl");
                    param.addNewValues().setUnique(epi2atl);

                    String normode = data.getParameters().get("normode");
                    param = parameters.addNewParameter();
                    param.setName("normode");
                    param.addNewValues().setUnique(normode);

                    String economy = data.getParameters().get("economy");
                    param = parameters.addNewParameter();
                    param.setName("economy");
                    param.addNewValues().setUnique(economy);

                    // note capitalization of parameter name
                    if (TurbineUtils.HasPassedParameter("Siemens_interleave", data)) {
                        String Siemens_interleave = data.getParameters().get("Siemens_interleave");
                        if (Siemens_interleave != null && !Siemens_interleave.equals("")) {
                            param = parameters.addNewParameter();
                            param.setName("Siemens_interleave");
                            param.addNewValues().setUnique(Siemens_interleave);
                        }
                    }

                    ArrayList<String> functionalScans = getCheckBoxSelections(data,pet,"functional_"+StdBuildLauncher.EPI, data.getParameters().getInt(StdBuildLauncher.EPI+"_rowcount"));
                    if (functionalScans.size() > 0) {
                        ArrayList<String> functional = getRunLabels(bold,irun,functionalScans);
                        // xnatPipelineLauncher.setParameter("fcbolds",functional);
                        param = parameters.addNewParameter();
                        param.setName("fcbolds");
                            values = param.addNewValues();
                        if (functional.size() == 1) {
                            values.setUnique(functional.get(0));
                        }else {
                            for (int i = 0; i < functional.size(); i++) {
                                values.addList(functional.get(i));
                            }
                        }
                        // xnatPipelineLauncher.setParameter("preprocessFunctional","1");
                        param = parameters.addNewParameter();
                        param.setName("preprocessFunctional");
                        param.addNewValues().setUnique("1");
                    }
                    createRunLabelAssignmentFile(buildDir+File.separator + pet.getLabel(), bold, irun, pet);
                }
                build = true;
            }

            if (build) {
                String paramFileName = getName(pipelineName);

                paramFileName += "_params_" + s + ".xml";

                String paramFilePath = saveParameters(buildDir+File.separator + pet.getLabel(),paramFileName,parameters);

                xnatPipelineLauncher.setParameterFile(paramFilePath);
                rtn = xnatPipelineLauncher.launch();
            }else rtn = true;

        }catch(Exception e) {
            logger.debug(e);
        }
        return rtn;
    }

    private boolean createRunLabelAssignmentFile(String rootpath, ArrayList bolds, ArrayList runlabels, XnatPetsessiondata pet) {
        boolean rtn = false;
        Writer output = null;
        File dir = new File(rootpath);
        if (!dir.exists()) dir.mkdirs();

        File file = new File(rootpath + File.separator + "scanmapping.txt");
        try {
            output = new BufferedWriter(new FileWriter(file));
            output.write("Folder \t Scan Number \t Scan Type" + "\n");
            for (int i = 0; i < bolds.size(); i++) {
                String scanid = (String)bolds.get(i);
                String runlabel = (String)runlabels.get(i);
                XnatImagescandata imageScan = pet.getScanById(scanid);
                output.write("bold" + runlabel + " \t " + scanid + " \t " + imageScan.getType() + "\n");
            }
        }catch(Exception e ) {
            logger.debug("Unable to save scanmapping file ",e);
        }finally {
            if (output != null) {
                try {
                    output.close();
                }catch(IOException ioe) {
                    logger.debug(ioe);
                }
            }
        }
        return rtn;
    }

    private ArrayList<String> getRunLabels(RunData data) {
        int totalCount = data.getParameters().getInt(StdBuildLauncher.EPI+"_rowcount");
        ArrayList<String> rtn = new ArrayList<String>();
        for (int i = 0; i < totalCount; i++) {
            if (TurbineUtils.HasPassedParameter(StdBuildLauncher.EPI+"_"+i, data)) {
                rtn.add(data.getParameters().get("boldrunlabel_" + StdBuildLauncher.EPI+"_"+i).trim());
            }
        }
        return rtn;
    }


    private ArrayList<String> getRunLabels(RunData data, String type) {
        ArrayList<String> rtn = null;
        int totalCount = data.getParameters().getInt(type+"_rowcount");
        rtn = new ArrayList<String>();
        for (int i = 0; i < totalCount; i++) {
            String label = data.getParameters().getString("boldrunlabel_"+type+"_"+i);
            if (label != null && !label.equals("")) {
                rtn.add(label);
            }
        }
        return rtn;
    }

    private ArrayList<String> getRunLabels(ArrayList<String> boldScans) {
        ArrayList<String> rtn = new ArrayList<String>();
        for (int i = 0; i < boldScans.size(); i++) {
            rtn.add("run"+(i+1));
        }
        return rtn;
    }

    private ArrayList<String> getRunLabels(ArrayList<String> boldScans,ArrayList<String> runLabels,ArrayList<String> functionalScans ) {
        ArrayList<String> rtn = new ArrayList<String>();
        for (int i = 0; i < functionalScans.size(); i++) {
            for (int j=0; j<boldScans.size(); j++) {
                if (boldScans.get(j).equals(functionalScans.get(i))) {
                    rtn.add(runLabels.get(j));
                    break;
                }
            }
        }
        return rtn;
    }

}
