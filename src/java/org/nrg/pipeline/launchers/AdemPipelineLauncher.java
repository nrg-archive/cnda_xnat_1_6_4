/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.nrg.pipeline.launchers;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.PipelineFileUtils;
import org.nrg.pipeline.utils.PipelineUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParameterData.Values;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.model.XnatMrsessiondataI;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;

public class AdemPipelineLauncher extends PipelineLauncher{
	static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AdemPipelineLauncher.class);

	public static final	String T1 = "T1";
	public static final	String T1_PARAM = "t1";
	
	public static final	String MPRAGE = "MPRAGE";
	public static final String MPRAGE_PARAM = "mprs";
	
	
	public static final	String T2 = "T2";
	public static final String T2_PARAM = "t2";

	
	public static final	String DTI = "DTI";
	public static final String DTI_PARAM = "dti";

	public static final	String FLAIR = "FLAIR";
	public static final	String FLAIR_PARAM = "flair";

	
	public boolean launch(RunData data, Context context) {
		boolean rtn = false;
		try {
		ItemI data_item = TurbineUtils.GetItemBySearch(data);
		XnatMrsessiondata mr = new XnatMrsessiondata(data_item);
		XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetLauncher(data, context, mr);
		String pipelineName = data.getParameters().get("pipelinename");
		String cmdPrefix = data.getParameters().get("cmdprefix");
		xnatPipelineLauncher.setPipelineName(pipelineName);
		String buildDir = PipelineFileUtils.getBuildDir(mr.getProject(), true);
		buildDir +=   "adem"  ;
		xnatPipelineLauncher.setBuildDir(buildDir);
		xnatPipelineLauncher.setNeedsBuildDir(false);
		
        Parameters parameters = Parameters.Factory.newInstance();

        //xnatPipelineLauncher.setParameter("isDicom", data.getParameters().get("isDicom"));
        ParameterData param = parameters.addNewParameter();
    	param.setName("isDicom");
    	param.addNewValues().setUnique(data.getParameters().get("isDicom"));
 
        param = parameters.addNewParameter();
    	param.setName("rm_prev_folder");
    	param.addNewValues().setUnique("0");

    	
		//xnatPipelineLauncher.setParameter("sessionId",mr.getLabel());
		param = parameters.addNewParameter();
    	param.setName("sessionId");
    	param.addNewValues().setUnique(mr.getLabel());

		String target = data.getParameters().get("target");
//		xnatPipelineLauncher.setParameter("target",target);
		param = parameters.addNewParameter();
    	param.setName("target");
    	param.addNewValues().setUnique(target);
		
//		xnatPipelineLauncher.setParameter("bVec",data.getParameters().get("bvec"));
		param = parameters.addNewParameter();
    	param.setName("bVec");
    	param.addNewValues().setUnique(data.getParameters().get("bvec"));
		
//		xnatPipelineLauncher.setParameter("ibmax",data.getParameters().get("ibmax"));
		param = parameters.addNewParameter();
    	param.setName("ibmax");
    	param.addNewValues().setUnique(data.getParameters().get("ibmax"));
	
//    	xnatPipelineLauncher.setParameter("r",data.getParameters().get("resolution"));
		param = parameters.addNewParameter();
    	param.setName("r");
    	param.addNewValues().setUnique(data.getParameters().get("resolution"));
		
		if (TurbineUtils.HasPassedParameter(PipelineUtils.GRAD_UNWARP, data)) {
			boolean grad_unwarp = data.getParameters().getBoolean(PipelineUtils.GRAD_UNWARP);
			//xnatPipelineLauncher.setParameter("grad_unwarp",grad_unwarp?"1":"0");
			param = parameters.addNewParameter();
	    	param.setName("grad_unwarp");
	    	param.addNewValues().setUnique(grad_unwarp?"1":"0");
		}

		if (TurbineUtils.HasPassedParameter(PipelineUtils.CROSS_DAY_REGISTER, data)) {
			String cross_day_register = data.getParameters().get(PipelineUtils.CROSS_DAY_REGISTER);
			if (!cross_day_register.equals("-1")) {
				XnatMrsessiondataI crossMr = XnatMrsessiondata.getXnatMrsessiondatasById(cross_day_register, TurbineUtils.getUser(data), false);
				if (crossMr != null) {
//					xnatPipelineLauncher.setParameter("sessionRef", ((XnatMrsessiondata)crossMr).getLabel());
					param = parameters.addNewParameter();
			    	param.setName("sessionRef");
			    	param.addNewValues().setUnique(((XnatMrsessiondata)crossMr).getLabel());
										
//				    xnatPipelineLauncher.setParameter("sessionRefPath", ((XnatMrsessiondata)crossMr).getArchivePath()+crossMr.getLabel());
					param = parameters.addNewParameter();
			    	param.setName("sessionRefPath");
			    	param.addNewValues().setUnique(((XnatMrsessiondata)crossMr).getArchivePath()+crossMr.getLabel());

				}
			}
		}
		
		ArrayList<String> mprs = getCheckBoxSelections(data,mr,AdemPipelineLauncher.MPRAGE);
		ArrayList<String> t1 = getCheckBoxSelections(data,mr,AdemPipelineLauncher.T1);
		ArrayList<String> t2s = getCheckBoxSelections(data,mr,AdemPipelineLauncher.T2);
		ArrayList<String> flair = getCheckBoxSelections(data,mr,AdemPipelineLauncher.FLAIR);
		ArrayList<String> dti = getCheckBoxSelections(data,mr,AdemPipelineLauncher.DTI);
		ArrayList<String> t1mprage = new ArrayList<String>();
		
		t1mprage.addAll(mprs); 
		t1mprage.addAll(t1);
		boolean build = false;

		if (TurbineUtils.HasPassedParameter("build_" + AdemPipelineLauncher.MPRAGE, data)) {
//			xnatPipelineLauncher.setParameter("mprs",mprs);
		      param = parameters.addNewParameter();
		      param.setName("mprs");
		      Values values = param.addNewValues();
		      if (mprs.size() == 1) {
		    	  values.setUnique(mprs.get(0));
		      }else {
			       for (int i = 0; i < mprs.size(); i++) {
			        	values.addList(mprs.get(i));
			        }
		      }
			build = true;
		}
		if (TurbineUtils.HasPassedParameter("build_" + AdemPipelineLauncher.T2, data)) {
//			xnatPipelineLauncher.setParameter("t2",t2s);
		      param = parameters.addNewParameter();
		      param.setName("t2");
		      Values values = param.addNewValues();
		      if (t2s.size() == 1) {
		    	  values.setUnique(t2s.get(0));
		      }else {
			      for (int i = 0; i < t2s.size(); i++) {
			        	values.addList(t2s.get(i));
			        }
		      }
			if (TurbineUtils.HasPassedParameter(PipelineUtils.COLLATE, data)) {
//	    		xnatPipelineLauncher.setParameter(PipelineUtils.COLLATE,"1");
				param = parameters.addNewParameter();
		    	param.setName(PipelineUtils.COLLATE);
		    	param.addNewValues().setUnique("1");
			}
			build = true;
		}

		if (TurbineUtils.HasPassedParameter("build_" + AdemPipelineLauncher.FLAIR, data)) {
//			xnatPipelineLauncher.setParameter("flair",flair);
		      param = parameters.addNewParameter();
		      param.setName("flair");
		      Values values = param.addNewValues();
		      if (flair.size() == 1) {
		    	  values.setUnique(flair.get(0));
		      }else {
			       for (int i = 0; i < flair.size(); i++) {
			        	values.addList(flair.get(i));
			        }
		      }
			build = true;
		}
		if (TurbineUtils.HasPassedParameter("build_" + AdemPipelineLauncher.DTI, data)) {
//			xnatPipelineLauncher.setParameter("dti",dti);
		      param = parameters.addNewParameter();
		      param.setName("dti");
		      Values values = param.addNewValues();
		      if (dti.size() == 1) {
		    	  values.setUnique(dti.get(0));
		      }else {
			       for (int i = 0; i < dti.size(); i++) {
			        	values.addList(dti.get(i));
			        }
		      }
			build = true;
		}
		//if (TurbineUtils.HasPassedParameter("build_" + StdBuildLauncher.T1, data)) {
//			xnatPipelineLauncher.setParameter("t1",t1mprage);
	      param = parameters.addNewParameter();
	      param.setName("t1");
	      Values values = param.addNewValues();
	      if (t1mprage.size() == 1) {
	    	  values.setUnique(t1mprage.get(0));
	      }else {
		      for (int i = 0; i < t1mprage.size(); i++) {
		        	values.addList(t1mprage.get(i));
		        }
	      }		
		//}
	    
	      
	    
		if (build) {
			String paramFileName = getName(pipelineName);
			Date date = new Date();
		    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		    String s = formatter.format(date);
			
			paramFileName += "_params_" + s + ".xml"; 
			String paramFilePath = saveParameters(buildDir+File.separator + mr.getLabel(),paramFileName,parameters);  
		    xnatPipelineLauncher.setParameterFile(paramFilePath);
			rtn = xnatPipelineLauncher.launch(cmdPrefix);
		}else
			rtn = true;
		
		if (TurbineUtils.HasPassedParameter(PipelineUtils.FREESURFER, data)) {
				boolean fsuccess = new FreesurferLauncher(mprs).launch(data, context, mr);
				rtn = rtn && fsuccess;
		}

		}catch(Exception e) {
			logger.debug(e); 
		}
		return rtn;
	}

}
