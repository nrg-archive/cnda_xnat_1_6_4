package org.nrg.pipeline.launchers;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.PipelineFileUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.nrg.xdat.om.FsFsdata;
import org.nrg.xdat.turbine.utils.TurbineUtils;

public class FreesurferRelauncher extends PipelineLauncher{
    static org.apache.log4j.Logger logger = Logger.getLogger(FreesurferRelauncher.class);
    final static String FS_51_RELAUNCH_PIPELINE_NAME = "Freesurfer/Freesurfer_relaunch.xml";
    final static String FS_51_VERSION = "freesurfer-Linux-centos4_x86_64-stable-pub-v5.1.0";
    final static String FS_53_RELAUNCH_PIPELINE_NAME = "Freesurfer/Freesurfer_5.3.xml";
    final static String FS_53_VERSION = "freesurfer-Linux-centos6_x86_64-stable-pub-v5.3.0-HCP-patch";

    FsFsdata fs;

    public FreesurferRelauncher(FsFsdata fs) {
        this.fs = fs;
    }


    public boolean launch(RunData data, Context context) {
        String fsVersion = fs.getFsVersion();
        if (fsVersion.equalsIgnoreCase(FS_51_VERSION)) {
            return launch_51(data, context);
        } else if (fsVersion.equalsIgnoreCase(FS_53_VERSION)) {
            return launch_53(data, context);
        }
        return false;
    }

    public boolean launch_53(RunData data, Context context) {
        boolean launch_success = false;
        try {
            XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetBareLauncherForExperiment(data, context, fs);
            xnatPipelineLauncher.setPipelineName(FS_53_RELAUNCH_PIPELINE_NAME);
            xnatPipelineLauncher.setSupressNotification(true);
            xnatPipelineLauncher.setId(fs.getImageSessionData().getId());
            xnatPipelineLauncher.setLabel(fs.getImageSessionData().getLabel());
            xnatPipelineLauncher.setDataType(fs.getImageSessionData().getXSIType());
            xnatPipelineLauncher.setExternalId(fs.getImageSessionData().getProject());

            String buildDir = PipelineFileUtils.getBuildDir(fs.getImageSessionData().getProject(), true);
            buildDir +=  "fsrfer";
            xnatPipelineLauncher.setBuildDir(buildDir);
            xnatPipelineLauncher.setNeedsBuildDir(false);

            Parameters parameters = Parameters.Factory.newInstance();

            ParameterData param = parameters.addNewParameter();
            param.setName("relaunch_in");
            param.addNewValues().setUnique("true");

            param = parameters.addNewParameter();
            param.setName("assessor_id_in");
            param.addNewValues().setUnique(fs.getId());

            String[] stringParams = new String[] {"recon-all_args","environment"};
            for (String paramName : stringParams) {
                if (TurbineUtils.HasPassedParameter(paramName, data)) {
                    param = parameters.addNewParameter();
                    param.setName(paramName);
                    param.addNewValues().setUnique((String) TurbineUtils.GetPassedParameter(paramName, data));
                }
            }


            // String emailsStr = TurbineUtils.getUser(data).getEmail() + "," + data.getParameters().get("emailField");
            // String[] emails = emailsStr.trim().split(",");
            // for (int i = 0 ; i < emails.length; i++) {
            //     if (emails[i]!=null && !emails[i].equals(""))  xnatPipelineLauncher.notify(emails[i]);
            // }
            String emailsCSV = TurbineUtils.getUser(data).getEmail() + ",cnda-ops@nrg.wustl.edu";
            for (String list : new String[] {"general-notif-list", "specific-notif-list"}) {
                if (TurbineUtils.HasPassedParameter(list, data)) {
                    String notifCSV = (String) TurbineUtils.GetPassedParameter(list, data);
                    for (String email : notifCSV.split(",")) {
                        if (StringUtils.isNotBlank(email) &&
                                !(email.contains("'") || email.contains("\"") || emailsCSV.contains(email))) {
                            emailsCSV += "," + email;
                        }
                    }
                }
            }
            // for (String email : emailsCSV.split(",")) {
            //     xnatPipelineLauncher.notify(email);
            // }
            param = parameters.addNewParameter();
            param.setName("email_csv_in");
            param.addNewValues().setUnique(emailsCSV);

            String paramFileName = getName(FS_53_RELAUNCH_PIPELINE_NAME);
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            String s = formatter.format(date);

            paramFileName += "_params_" + s + ".xml";

            String paramFilePath = saveParameters(buildDir+File.separator + fs.getImageSessionData().getLabel(),paramFileName,parameters);

            xnatPipelineLauncher.setParameterFile(paramFilePath);
            launch_success = xnatPipelineLauncher.launch();

        } catch(Exception e) {
            logger.error(e);
        }

        return launch_success ;
    }

    public boolean launch_51(RunData data, Context context) {
        boolean launch_success = false;
        try {
            XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetBareLauncherForExperiment(data, context, fs);
            xnatPipelineLauncher.setPipelineName(FS_51_RELAUNCH_PIPELINE_NAME);
            xnatPipelineLauncher.setSupressNotification(true);
            xnatPipelineLauncher.setId(fs.getId());
            xnatPipelineLauncher.setLabel(fs.getLabel());
            xnatPipelineLauncher.setDataType(fs.getXSIType());
            xnatPipelineLauncher.setExternalId(fs.getProject());

            String buildDir = PipelineFileUtils.getBuildDir(fs.getProject(), true);
            buildDir +=  "fsrfer";
            xnatPipelineLauncher.setBuildDir(buildDir);
            xnatPipelineLauncher.setNeedsBuildDir(false);

            Parameters parameters = Parameters.Factory.newInstance();

            ParameterData param = parameters.addNewParameter();
            param.setName("fsId");
            param.addNewValues().setUnique(fs.getId());

            param = parameters.addNewParameter();
            param.setName("fsLabel");
            param.addNewValues().setUnique(fs.getLabel());


            param = parameters.addNewParameter();
            param.setName("subjectlabel");
            param.addNewValues().setUnique(fs.getImageSessionData().getSubjectData().getLabel());

            param = parameters.addNewParameter();
            param.setName("mrLabel");
            param.addNewValues().setUnique(fs.getImageSessionData().getLabel());

            param = parameters.addNewParameter();
            param.setName("mrId");
            param.addNewValues().setUnique(fs.getImageSessionData().getId());

            // param = parameters.addNewParameter();
            // param.setName("archivePath");
            // param.addNewValues().setUnique(fs.getImageSessionData().getArchivePath()+ fs.getImageSessionData().getLabel() + File.separator +"ASSESSORS"+ File.separator + fs.getArchiveDirectoryName());


            // param = parameters.addNewParameter();
            // param.setName("project");
            // param.addNewValues().setUnique(fs.getProject());

            String emailsStr = TurbineUtils.getUser(data).getEmail() + "," + data.getParameters().get("emailField");
            String[] emails = emailsStr.trim().split(",");
            for (int i = 0 ; i < emails.length; i++) {
                if (emails[i]!=null && !emails[i].equals(""))  xnatPipelineLauncher.notify(emails[i]);
            }

            String paramFileName = getName(FS_51_RELAUNCH_PIPELINE_NAME);
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            String s = formatter.format(date);

            paramFileName += "_params_" + s + ".xml";

            String paramFilePath = saveParameters(buildDir+File.separator + fs.getLabel(),paramFileName,parameters);

            xnatPipelineLauncher.setParameterFile(paramFilePath);
            launch_success = xnatPipelineLauncher.launch();

        }catch(Exception e) {
            logger.error(e);
        }

        return launch_success ;

    }
}
