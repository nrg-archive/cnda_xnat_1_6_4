package org.apache.turbine.app.cnda_xnat.modules.screens;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xnat.turbine.modules.screens.DefaultPipelineScreen;

public class PipelineScreen_RelaunchFreesurferAfterManualUpload extends DefaultPipelineScreen{
	static Logger logger = Logger.getLogger(PipelineScreen_RelaunchFreesurferAfterManualUpload.class);
	public void finalProcessing(RunData data, Context context){
		try {
			// XnatMrsessiondata mr = (XnatMrsessiondata) om;
			// context.put("mr", mr);
		}catch(Exception e) {
			logger.error("Unable to launch pipeline", e);
			e.printStackTrace();
		}
	}
}
