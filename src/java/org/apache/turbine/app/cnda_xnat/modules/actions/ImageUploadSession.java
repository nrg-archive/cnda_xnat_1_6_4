/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.apache.turbine.app.cnda_xnat.modules.actions;

import ij.io.FileInfo;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ParameterParser;
import org.apache.velocity.context.Context;
import org.nrg.plexiViewer.Reader.AnalyzeObjectMapReader;
import org.nrg.xdat.om.XnatImageresource;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.om.XnatRegionresource;
import org.nrg.xdat.om.XnatRegionresourceLabel;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.modules.actions.SecureAction;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFT;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.FileUtils;
import org.nrg.xft.utils.zip.TarUtils;
import org.nrg.xft.utils.zip.ZipI;
import org.nrg.xft.utils.zip.ZipUtils;

public class ImageUploadSession extends SecureAction {

    static org.apache.log4j.Logger logger = Logger.getLogger(ImageUploadSession.class);
    
    /* Send in 
     * ID: sessionId
     * relative_path: path relative to the session folder where the uploaded file is to be stored
     * image_archive: form file input type name
     *  (non-Javadoc)
     * @see org.apache.turbine.modules.actions.VelocityAction#doPerform(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
     */
    
    public void doPerform(RunData data, Context context) throws Exception {

        
        ParameterParser params = data.getParameters();

        HttpSession session = data.getSession();

        String sessionID= null;

        if (params.get("ID")!=null && !params.get("ID").equals("")){

            sessionID=params.get("ID");

            session.setAttribute(sessionID + "Upload", new Integer(0));

            session.setAttribute(sessionID + "Extract", new Integer(0));

        }
        
        String relativePathToSaveFile = params.get("relative_path");
        
        if (relativePathToSaveFile == null) {
            data.setMessage("Path to save uploaded file to not specified. Please contact system administrator " + XFT.GetAdminEmail());
            data.setScreenTemplate("ImageUploadGeneric.vm");
            return;
        }
        
        if (relativePathToSaveFile.startsWith(File.separator)) {
            relativePathToSaveFile = relativePathToSaveFile.substring(1);
        }
        ItemI item = TurbineUtils.GetItemBySearch(data,false);
        String path = null;
        boolean isPet = false;
        try {
            XnatPetsessiondata pet = new XnatPetsessiondata(item);
            path = pet.getArchivePath() +  pet.getLabel() + File.separator + relativePathToSaveFile;
            isPet = true;
        }catch(ClassCastException cce) {}
        
        if (!isPet) {
	        try {
	            XnatMrsessiondata mr = new XnatMrsessiondata(item);
	            path = mr.getArchivePath() + mr.getLabel() + File.separator + relativePathToSaveFile;
	        }catch(ClassCastException cce) {logger.error(cce);}
        }
        if (path == null) {
            data.setMessage("Path to save uploaded file could not be found. Please contact system administrator " + XFT.GetAdminEmail());
            data.setScreenTemplate("ImageUploadSession.vm");
            return;
        }


        if (sessionID!=null)session.setAttribute(sessionID + "Upload", new Integer(0));

            try {

                FileItem fi = params.getFileItem("image_archive");

                if (fi != null)

                {

                    String filename = fi.getName();

                    String compression_method = ".zip";

                    if (filename.indexOf(".")!=-1){

                        compression_method = filename.substring(filename.lastIndexOf("."));

                    }

                    File dir = new File(path);

                    if (!dir.exists()){

                        dir.mkdirs();

                    }

                    System.out.println("Uploading file.");

                    //upload file

                    InputStream is = fi.getInputStream();

                    if (sessionID!=null)session.setAttribute(sessionID + "Upload", new Integer(100));

                    System.out.println("Extracting file.");

                    ZipI zipper = null;

                    if (compression_method.equalsIgnoreCase(".tar")){

                        zipper = new TarUtils();

                    }else if (compression_method.equalsIgnoreCase(".gz")){

                        zipper = new TarUtils();

                        zipper.setCompressionMethod(ZipOutputStream.DEFLATED);

                    }else{

                        zipper = new ZipUtils();

                    }
                    ArrayList extractedFiles = zipper.extract(is,path);
                    if (sessionID!=null)session.setAttribute(sessionID + "Extract", new Integer(100));
                    
                    System.out.println("File Upload Complete. File uploaded to " + path + " No of files extracted " + extractedFiles.size());
                    
                    if (isPet) {
                        XnatPetsessiondata pet = new XnatPetsessiondata(item);
                        XnatPetsessiondata pet1 = new XnatPetsessiondata((UserI)TurbineUtils.getUser(data));
                        pet1.setId(pet.getId());
                        pet1.setLabel(pet.getLabel());
                        pet1.setProject(pet.getProject());
                        ArrayList miscFiles = addRegions(data,pet1,extractedFiles);
                        data.getSession().setAttribute("miscFiles", miscFiles);
                        data.getSession().setAttribute("pet_image_upload",pet1);
                        data.setRedirectURI(TurbineUtils.GetRelativeServerPath(data) + "" + "/app/template/PetObjectImageUpload.vm/search_value/"+item.getProperty("ID") + "/search_element/xnat:petSessionData/search_field/xnat:petSessionData.ID/popup/true");
                    }   
                }
            }catch(Exception e) {
                logger.error("",e);

                data.setMessage(e.getMessage());

                data.setScreenTemplate("Error.vm");

                data.getParameters().add("exception", e.toString());

                return;
                
            }

    }
    
    private ArrayList addRegions(RunData data, XnatPetsessiondata pet, ArrayList extractedFiles) {
        ArrayList miscFiles = new ArrayList();
        if (extractedFiles != null && extractedFiles.size() > 0 ) {
            XDATUser user = TurbineUtils.getUser(data);
            for (int i = 0; i < extractedFiles.size(); i++) {
                File f = (File)extractedFiles.get(i);
                if (f.isFile()) {
                    if (f.getName().endsWith(".obj")) {
                        AnalyzeObjectMapReader objReader = new AnalyzeObjectMapReader(f.getAbsolutePath());
                        String[] regionLabels = objReader.getRegionLabels();
                        XnatRegionresource regionRsc = new XnatRegionresource((UserI)user);
                        regionRsc.setSessionId(pet.getId());
                        regionRsc.setName(f.getName().substring(0,f.getName().length()-4));
                        regionRsc.setHemisphere("both");
                        for (int j = 1; j < regionLabels.length; j++ ) {
                            XnatRegionresourceLabel lbl = new XnatRegionresourceLabel((UserI)user);
                            lbl.setId(new Integer(j));
                            lbl.setLabel(regionLabels[j]);
                            if (lbl.getLabel().endsWith("_L") || lbl.getLabel().endsWith("_l") ) {
                                lbl.setHemisphere("left");
                                //lbl.setLabel(lbl.getLabel().substring(0,lbl.getLabel().length()-2));
                            }else if (lbl.getLabel().endsWith("_R") || lbl.getLabel().endsWith("_r") ) {
                                lbl.setHemisphere("right");
                                //lbl.setLabel(lbl.getLabel().substring(0,lbl.getLabel().length()-2));
                            }else  {
                                lbl.setHemisphere("both");
                            }
                            try {regionRsc.setSubregionlabels_label(lbl);}
                            catch(Exception e) {logger.error("",e);}
                        }
                        XnatImageresource imgRsc = new XnatImageresource((UserI)user);
                        //System.out.println(f.getAbsolutePath() + "  " + f.getAbsolutePath().replaceAll("\\\\","/") + "  " + XFT.GetArchiveRootPath());
                      //  imgRsc.setUri(FileUtils.RemoveRootPath(f.getAbsolutePath().replaceAll("\\\\","/"),pet.getArchiveRootPath()));
                       imgRsc.setUri(f.getAbsolutePath().replaceAll("\\\\","/"));
                        imgRsc.setContent("ANALYZE OBJECT MAP");
                        imgRsc.setFormat("ANALYZE_OBJ");
                        FileInfo fi = objReader.getFileInfo();
                        imgRsc.setDimensions_x(fi.width);
                        imgRsc.setDimensions_y(fi.height);
                        imgRsc.setDimensions_z(fi.nImages);
                        imgRsc.setDimensions_volumes(1);
                        try {regionRsc.setFile((ItemI)imgRsc);}catch(Exception e){logger.error("",e);}
                        regionRsc.setCreator_firstname(user.getFirstname());
                        regionRsc.setCreator_lastname(user.getLastname());
                        try {
                            pet.setRegions_region(regionRsc);
                        }catch(Exception e) {logger.error("",e);}
                    }else 
                        miscFiles.add(f);
                }
            }
        }
        return miscFiles;
    }
}
