package org.apache.turbine.app.cnda_xnat.modules.screens;

import java.util.ArrayList;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.schema.SchemaElement;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.collections.ItemCollection;
import org.nrg.xft.schema.design.SchemaElementI;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xnat.turbine.modules.screens.DefaultPipelineScreen;

public class PipelineScreen_PetProcessing extends DefaultPipelineScreen  {

	private ArrayList freesurferMrs;

    public void finalProcessing(RunData data, Context context)
    {
        XnatImagesessiondata pet = new XnatImagesessiondata(item);
        freesurferMrs = new ArrayList();

        if (pet.getScans_scan() == null || pet.getScans_scan().size() == 0) {
            data.setMessage("No files uploaded for the session. Please upload files first");
            return;
        }
        
        setFreesurferItems(data,pet.getSubjectId());

    	context.put("projectSettings", projectParameters);
        context.put("pet", pet);
        context.put("freesurfers", freesurferMrs);
        context.put("isstat", isstaticScan(pet));

    }
    
    private int isstaticScan(XnatImagesessiondata pet) {
    	int rtn = 2;
    	if (pet instanceof XnatPetsessiondata) {
    		XnatPetsessiondata petSession = (XnatPetsessiondata)pet;
    		Object startTime = petSession.getStartTimeScan();
    		Object injectionTime = petSession.getStartTimeInjection();
    		System.out.println(startTime.getClass().getName() + ":" + injectionTime.getClass().getName());
    	}
    	return rtn;
    }

    private void setFreesurferItems(RunData data, String search_value) {
    	XnatImagesessiondata om = null;
    	try {
            ItemSearch search = new ItemSearch();
            search.setUser(TurbineUtils.getUser(data));
            String elementName = "xnat:imageSessionData";
            SchemaElementI gwe = SchemaElement.GetElement(elementName);
            search.setElement(elementName);
            search.addCriteria("xnat:imageSessionData.subject_ID",search_value);
            boolean b = false;
            b= gwe.isPreLoad();
            search.setAllowMultiples(b);
            ItemCollection items = search.exec();
            ArrayList mrsByDate = items.getItems("xnat:imageSessionData.Date","DESC");
            if (mrsByDate.size() > 0)
            {
                for (int i =0; i <mrsByDate.size(); i++) {
                    ItemI item = (ItemI)mrsByDate.get(i);
                    om = (XnatImagesessiondata)BaseElement.GetGeneratedItem(item);
                    ArrayList fsAssessors = om.getAssessors("fs:fsData");
                    if (fsAssessors.size() > 0 ) {
                    	//MR Session has Freesurfer data
                        freesurferMrs.add(om);
                    }
                }
            }
        }catch(Exception e) {e.printStackTrace();}
    }

}
