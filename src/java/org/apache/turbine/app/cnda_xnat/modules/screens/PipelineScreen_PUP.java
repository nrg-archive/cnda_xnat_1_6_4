package org.apache.turbine.app.cnda_xnat.modules.screens;

import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.nrg.config.entities.Configuration;
import org.nrg.pipeline.PipelineRepositoryManager;
import org.nrg.pipeline.utils.PipelineFileUtils;
import org.nrg.pipeline.xmlbeans.PipelineData;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.bean.CatCatalogBean;
import org.nrg.xdat.model.*;
import org.nrg.xdat.om.*;
import org.nrg.xdat.om.base.BaseXnatProjectdata;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xnat.turbine.modules.screens.DefaultPipelineScreen;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.nrg.xnat.utils.CatalogUtils;

public class PipelineScreen_PUP extends DefaultPipelineScreen  {
    public final String PIPELINE_NAME = "PETUnifiedPipeline";
    public final String PARAMS_CONFIG_TOOL_NAME = "pipelines";
    public final String PARAMS_CONFIG_FILE_NAME = PIPELINE_NAME + "_default_params";

    public final String SCANTYPE_CONFIG_TOOL_NAME = "params";
    public final String SCANTYPE_CONFIG_FILE_NAME = "mr_scan_types";

    public final String FILTERMAP_CONFIG_TOOL_NAME = "pipelines";
    public final String FILTERMAP_CONFIG_FILE_NAME = PIPELINE_NAME + "_scanner_filter_map";

    public final String HLMAP_CONFIG_TOOL_NAME = "pipelines";
    public final String HLMAP_CONFIG_FILE_NAME = PIPELINE_NAME + "_tracer_hl_map";


    private String _projectId;
    private String mrScanType;

    private ObjectMapper mapper = new ObjectMapper();

    public void finalProcessing(RunData data, Context context) {
        Map<String,Object> objectsWeNeed = new HashMap<String,Object>();

        XnatImagesessiondata pet = new XnatImagesessiondata(item);
        context.put("pet", pet);

        _projectId = pet.getProject();

        try {
            mrScanType = getConfig(SCANTYPE_CONFIG_TOOL_NAME, SCANTYPE_CONFIG_FILE_NAME,_projectId);
        } catch (Exception e) {
            data.setMessage("An error has occurred. Could not find MR scan types from config. "+e.getMessage());
        }
        List<XnatImagescandataI> petSessionScans = pet.getScans_scan();
        if (petSessionScans == null || petSessionScans.size() == 0) {
            data.setMessage("No files uploaded for the session. Please upload files first");
            return;
        }

        // Session-level params
        objectsWeNeed.put("sessionId", pet.getId());
        objectsWeNeed.put("sessionLabel", pet.getLabel());

        String tracer = "";
        if (pet.getXSIType().equalsIgnoreCase("xnat:petSessionData")) {
            tracer = (new XnatPetsessiondata(item)).getTracer_name();
        } else if (pet.getXSIType().equalsIgnoreCase("xnat:petmrSessionData")) {
            tracer = (new XnatPetmrsessiondata(item)).getTracer_name();
        }
        objectsWeNeed.put("tracer",tracer);

        try {
            String half_life = getHalfLifeFromTracer(tracer);
            objectsWeNeed.put("half_life",half_life);
        } catch (Exception e) {
            data.setMessage("An error has occurred. "+e.getMessage());
            return;
        }

        // Scan-level params
        List<Map<String,String>> petScans = new ArrayList<Map<String, String>>();
        for (XnatImagescandataI scan : petSessionScans) {
            if (scan.getXSIType().equalsIgnoreCase("xnat:petScanData")) {
                Map<String,String> scanInfo = new HashMap<String, String>();

                XnatPetscandata petScan = (XnatPetscandata) scan;

                String scanner = StringUtils.isBlank(scan.getScanner_model()) ? "" : scan.getScanner_model();
                try {
                    Map<String,String> filterParams = getFilterParamsFromScanner(scanner);
                    scanInfo.put("filterxy",filterParams.get("filterxy"));
                    scanInfo.put("filterz",filterParams.get("filterz"));
                } catch (Exception e) {
                    data.setMessage("An error has occurred. "+e.getMessage());
                    return;
                }

                // If the scan does not have a DICOM resource, assume ECAT
                boolean hasDICOM = false;
                for ( XnatAbstractresourceI scanResource : scan.getFile() ) {

                    if (StringUtils.isNotBlank(scanResource.getLabel()) && scanResource.getLabel().equalsIgnoreCase("DICOM")) {
                        hasDICOM = true;
                        break;
                    }
                }
                if (hasDICOM) {
                    scanInfo.put("format","0");
                    scanInfo.put("format_type","DICOM");
                } else {
                    scanInfo.put("format","1");
                    scanInfo.put("format_type","_null_");
                }
                scanInfo.put("id",petScan.getId());
                scanInfo.put("label",petScan.getType());
                petScans.add(scanInfo);
            }
        }
        objectsWeNeed.put("petScanId",petScans);

        // Subject-level params
        try {
            XnatSubjectdata subject = new XnatSubjectdata(ItemSearch.GetItem("xnat:subjectData.ID", pet.getSubjectId(), TurbineUtils.getUser(data), false));
            objectsWeNeed.put("subject",subject.getLabel());
            objectsWeNeed.put("subjectResources",getSubjectResources(subject));
        } catch (Exception e) {
            data.setMessage("An error has occurred. Could not find subject resources. "+e.getMessage());
            return;
        }

        // Project param defaults
        try{
            objectsWeNeed.put("projectParams", getProjectParamDefaults());
        } catch (Exception e) {
            data.setMessage("An error has occurred. Could not find project parameters. "+e.getMessage());
            return;
        }

        // Write json, put into velocity context
        try {
            context.put("jsonWeNeed", mapper.writeValueAsString(objectsWeNeed));
        } catch (Exception e) {
            data.setMessage("Error rendering Java objects into JSON. "+e.getMessage());
        }
    }

    public List<Map<String,Object>> getSubjectResources(XnatSubjectdata subject) throws Exception {
        String REGISTERED_MR_RESOURCE_NAME_ROOT = "REGISTERED_";
        String CUSTOM_ROI_RESOURCE_NAME_ROOT = "ROI_";
        String ROI_IMG_FORMAT= "\\.4dfp\\.img";
        String ROI_TEXT_FORMAT= "\\.txt";

        List<Map<String,Object>> resources = new ArrayList<Map<String,Object>>();

        for (XnatSubjectassessordataI expt : subject.getExperiments_experiment()) {

            List<XnatImagescandata> scans = getMRScansByType(expt,mrScanType);
            if (scans == null || scans.size() == 0) {
                continue;
            }

            // Now that we know we have MR scans, we can cast to Imagesession
            XnatImagesessiondata imageSession = (XnatImagesessiondata)expt;
            String rootPath = imageSession.getArchiveRootPath();

            // Get info that all MR sessions will have
            Map<String,Object> sessionInfo = new HashMap<String,Object>();
            sessionInfo.put("mrlabel",imageSession.getLabel());
            sessionInfo.put("mrid",imageSession.getId());
            sessionInfo.put("mrdate",imageSession.getDate().toString());

            // Check for Freesurfers
            List<Map<String,String>> sessionFSInfo = new ArrayList<Map<String,String>>();
            for (XnatImageassessordataI assessor : imageSession.getAssessors_assessor()) {
                if (assessor.getXSIType().equalsIgnoreCase("fs:fsData")) {
                    FsFsdata fsAssessor = (FsFsdata)assessor;
                    Map<String,String> fsInfo = new HashMap<String,String>();
                    fsInfo.put("fsid",fsAssessor.getId());
                    fsInfo.put("fsdate",fsAssessor.getDate().toString());
                    fsInfo.put("fsversion",fsAssessor.getFsVersion());

                    if (fsAssessor.getValidation() == null || StringUtils.isBlank(fsAssessor.getValidation().getStatus())) {
                        fsInfo.put("qcstatus","No QC");
                    } else {
                        fsInfo.put("qcstatus", fsAssessor.getValidation().getStatus());
                    }

                    sessionFSInfo.add(fsInfo);
                }
            }
            sessionInfo.put("freesurfers",sessionFSInfo);

            // Check for registered MRs and hand-drawn ROIs
            List<Map<String,String>> sessionCustomROIInfo = new ArrayList<Map<String,String>>();
            for (XnatImagescandata scan : scans) {
                Map<String,Map<String,String>> resourceAtlases = new HashMap<String, Map<String, String>>();

                for ( XnatAbstractresourceI scanResource : scan.getFile() ) {
                    String scanResourceLabel = scanResource.getLabel();
                    if (!(scanResource instanceof XnatResourcecatalog)) {
                        continue;
                    }
                    XnatResourcecatalog scanResCat = ((XnatResourcecatalog) scanResource);

                    if (scanResourceLabel.matches(REGISTERED_MR_RESOURCE_NAME_ROOT + ".*")) {
                        String atlas = scanResourceLabel.substring(REGISTERED_MR_RESOURCE_NAME_ROOT.length());
                        Map<String,String> resourceAtlasInfo = resourceAtlases.get(atlas);
                        if (resourceAtlasInfo == null) {
                            resourceAtlasInfo = new HashMap<String, String>();
                        }
                        resourceAtlasInfo.put("hasRegisteredMR","true");
                        resourceAtlases.put(atlas,resourceAtlasInfo);
                    }

                    if (scanResourceLabel.matches(CUSTOM_ROI_RESOURCE_NAME_ROOT + ".*")) {
                        String atlas = scanResourceLabel.substring(CUSTOM_ROI_RESOURCE_NAME_ROOT.length());

                        // We have found an ROI_{atlas} resource. Now check if it has the files we need.
                        CatCatalogBean customROICatalog = CatalogUtils.getCatalog(rootPath, scanResCat);
                        CatEntryI[] roiImgFiles = CatalogUtils.getEntriesByRegex(customROICatalog, ".*" + ROI_IMG_FORMAT).toArray(new CatEntryI[]{});
                        if (roiImgFiles.length == 0) {
                            continue;
                        } else if (roiImgFiles.length > 1) {
                            throw new Exception("Found more than one ROI img file for session " + imageSession.getLabel() + ", scan " + scan.getId() + ", resource " + scanResourceLabel);
                        }

                        CatEntryI[] roiTextFiles = CatalogUtils.getEntriesByRegex(customROICatalog, ".*" + ROI_TEXT_FORMAT).toArray(new CatEntryI[]{});
                        if (roiTextFiles.length == 0) {
                            continue;
                        } else if (roiTextFiles.length > 1) {
                            throw new Exception("Found more than one ROI txt file for session " + imageSession.getLabel() + ", scan " + scan.getId() + ", resource " + scanResourceLabel);
                        }

                        Map<String,String> resourceAtlasInfo = resourceAtlases.get(atlas);
                        if (resourceAtlasInfo == null) {
                            resourceAtlasInfo = new HashMap<String, String>();
                        }
                        resourceAtlasInfo.put("hasCustomROI","true");
                        resourceAtlasInfo.put("roiimg",roiImgFiles[0].getName().replaceAll(ROI_IMG_FORMAT,""));
                        resourceAtlasInfo.put("roitextname",roiTextFiles[0].getName());
                        resourceAtlases.put(atlas,resourceAtlasInfo);
                    }
                }
                for (Map.Entry<String,Map<String,String>> atlasEntry : resourceAtlases.entrySet()) {
                    String atlas = atlasEntry.getKey();
                    Map<String,String> atlasInfo = atlasEntry.getValue();

                    String hasCustomROI = atlasInfo.get("hasCustomROI");
                    String hasRegisteredMR = atlasInfo.get("hasRegisteredMR");
                    if (!( hasCustomROI==null || hasRegisteredMR==null || hasCustomROI.isEmpty() || hasRegisteredMR.isEmpty())) {
                        Map<String, String> customROIInfo = new HashMap<String, String>();
                        customROIInfo.put("scanid", scan.getId());
                        customROIInfo.put("atltarg", atlas);
                        customROIInfo.put("roiimg", atlasInfo.get("roiimg"));
                        customROIInfo.put("text", atlasInfo.get("roitextname"));
                        sessionCustomROIInfo.add(customROIInfo);
                    }
                }
            }
            sessionInfo.put("customroi",sessionCustomROIInfo);

            resources.add(sessionInfo);
        }

        return resources;
    }

    public List<XnatImagescandata> getMRScansByType(XnatSubjectassessordataI expt, String mrScanType) throws Exception {
        List<XnatImagescandata> _return = null;

        String xsi = expt.getXSIType();
        String mrXsi = "xnat:mrSessionData";
        String petXsi = "xnat:petSessionData";
        String petmrXsi = "xnat:petmrSessionData";

        String[] types = mrScanType.split(",");
        if (types.length > 0 && (xsi.equalsIgnoreCase(mrXsi) || xsi.equalsIgnoreCase(petXsi) || xsi.equalsIgnoreCase(petmrXsi))) {
            _return = new ArrayList<XnatImagescandata>();
            for(String type : types) {
                ArrayList<XnatImagescandata> scanList = ((XnatImagesessiondata)expt).getScansByType(type.trim());
                if (scanList.size() > 0 ) {
                    _return.addAll(scanList);
                }
            }
        }
        return _return;
    }

    public String getHalfLifeFromTracer(String tracer) throws Exception {
        String half_life_config = getConfig(HLMAP_CONFIG_TOOL_NAME,HLMAP_CONFIG_FILE_NAME);
        Map<String,String> half_life_map = mapper.readValue(half_life_config, new TypeReference<Map<String,String>>() {});
        String half_life = half_life_map.get(tracer);
        if (StringUtils.isBlank(half_life)) {
            throw new Exception("Could not find half life from tracer "+tracer);
        }
        return half_life;
    }

    public Map<String,String> getFilterParamsFromScanner(String scanner) throws Exception {
        String filterConfig = getConfig(FILTERMAP_CONFIG_TOOL_NAME,FILTERMAP_CONFIG_FILE_NAME);
        Map<String,Map<String,String>> filterMap = mapper.readValue(filterConfig, new TypeReference<Map<String,Map<String,String>>>() {});
        Map<String,String> filterParams = filterMap.get(scanner);
        if (filterParams==null || !(filterParams.containsKey("filterxy") && filterParams.containsKey("filterz"))) {
            throw new Exception("Could not find filterxy,filterz from scanner "+scanner);
        }
        return filterParams;
    }

    public String getConfig(String configToolName, String configFileName, String project) throws Exception {
        Configuration config = XDAT.getConfigService().getConfig(configToolName, configFileName, BaseXnatProjectdata.getProjectInfoIdFromStringId(project));
        if (config == null || config.getContents() == null) {
            throw new Exception("Cannot find config at /projects/"+project+"/config/"+configToolName+"/"+configFileName);
        }
        return config.getContents().trim();
    }

    public String getConfig(String configToolName, String configFileName) throws Exception {
        Configuration config = XDAT.getConfigService().getConfig(configToolName, configFileName);
        if (config == null || config.getContents() == null) {
            throw new Exception("Cannot find config at /config/"+configToolName+"/"+configFileName);
        }
        return config.getContents().trim();
    }

    public Map<String,Object> getProjectParamDefaults() throws Exception {
        Map<String,Object> projectParameters;

        Configuration projectParamConfig = XDAT.getConfigService().getConfig(PARAMS_CONFIG_TOOL_NAME, PARAMS_CONFIG_FILE_NAME, BaseXnatProjectdata.getProjectInfoIdFromStringId(_projectId));
        if (projectParamConfig == null) {
            projectParameters = new HashMap<String,Object>();

            Map<String,Object> pipelineDetailsMap = getPipelineDetailsMap();
            List<Map<String,Object>> unformattedProjectParams = (List<Map<String,Object>>)pipelineDetailsMap.get("inputParameters");

            for (Map<String,Object> aParam : unformattedProjectParams) {
                Map<String,String> aValue = (Map<String,String>)aParam.get("values");
                if (aValue.containsKey("csv")) { // Don't need to add the "schemaLink" values. We'll get those at launch from the context.
                    Map<String,String> formattedParam = new HashMap<String, String>();
                    formattedParam.put("default",aValue.get("csv"));
                    formattedParam.put("type","text");
                    projectParameters.put((String)aParam.get("name"),formattedParam);
                }

            }
        } else {
            String projectParamString = projectParamConfig.getContents().trim();
            projectParameters = mapper.readValue(projectParamString, new TypeReference<Map<String,Object>>() {});
        }

        return projectParameters;
    }

    public Map<String,Object> getPipelineDetailsMap() throws Exception {
        ArcProject arcProject = ArcSpecManager.GetFreshInstance().getProjectArc(_projectId);
        PipePipelinerepository pipelineRepository = PipelineRepositoryManager.GetInstance();
        String pipelineDescriptorPath = "";
        for (String[] pipelineProperties : pipelineRepository.listPipelines(arcProject)) {
            if (pipelineProperties[2].equals(PIPELINE_NAME) || pipelineProperties[7].equals(PIPELINE_NAME)) {
                pipelineDescriptorPath = pipelineProperties[4];
                break;
            }
        }
        if (StringUtils.isBlank(pipelineDescriptorPath)) {
            throw new Exception("Pipeline does not exist in project");
        }

        PipePipelinedetails pipeline = pipelineRepository.getPipeline(pipelineDescriptorPath);
        PipelineData pipelineData = PipelineFileUtils.GetDocument(pipelineDescriptorPath).getPipeline();

        String xsiTypeAppliesTo = pipeline.getAppliesto();
        ArcPipelinedataI projectPipelineData = null;
        if (xsiTypeAppliesTo.equals(XnatProjectdata.SCHEMA_ELEMENT_NAME)) {
            projectPipelineData = arcProject.getPipelineByPath(pipelineDescriptorPath);
        } else {
            projectPipelineData = arcProject.getPipelineForDescendantByPath(xsiTypeAppliesTo,pipelineDescriptorPath);
        }
        if (projectPipelineData == null) {
            throw new Exception("Could not find project pipeline");
        }

        // Build hash map
        Map<String,Object> pipelineDetails = new HashMap<String,Object>();

        // Basic info
        pipelineDetails.put("path",pipelineDescriptorPath);
        pipelineDetails.put("description",pipelineData.getDescription());
        pipelineDetails.put("generates",pipelineRepository.getElementsGeneratedBy(pipeline));
        pipelineDetails.put("appliesTo",pipelineRepository.getDisplayName(xsiTypeAppliesTo));

        if (pipelineData.isSetResourceRequirements()) {
            String resourceRequirements = "";
            for (PipelineData.ResourceRequirements.Property property : pipelineData.getResourceRequirements().getPropertyArray()) {
                resourceRequirements += property.getName() + ", ";
            }
            if (resourceRequirements.endsWith(", ")) {
                int index = resourceRequirements.lastIndexOf(", ");
                resourceRequirements = resourceRequirements.substring(0, index);
            }

            pipelineDetails.put("resourceRequirements",resourceRequirements);
        }

        if (pipelineData.isSetDocumentation()) {
            PipelineData.Documentation doc = pipelineData.getDocumentation();
            if (doc.isSetWebsite()) {
                pipelineDetails.put("website",doc.getWebsite());
            }
            if (doc.isSetPublications()) {
                pipelineDetails.put("publications",doc.getPublications().getPublicationArray());
            }
            if (doc.isSetAuthors()) {
                PipelineData.Documentation.Authors.Author[] authors = doc.getAuthors().getAuthorArray();

                List<Map<String,String>> authorInfoList = new ArrayList<Map<String,String>>();
                for (PipelineData.Documentation.Authors.Author aAuthor : authors) {
                    Map<String,String> authorInfo = new HashMap<String,String>();
                    authorInfo.put("firstname",aAuthor.getFirstname());
                    authorInfo.put("lastname",aAuthor.getLastname());
                    if (aAuthor.isSetContact()) {
                        PipelineData.Documentation.Authors.Author.Contact contact = aAuthor.getContact();
                        if (contact.isSetEmail()) {
                            authorInfo.put("email",contact.getEmail());
                        }
                        if (contact.isSetPhone()) {
                            authorInfo.put("phone",contact.getPhone());
                        }
                    }
                    authorInfoList.add(authorInfo);
                }
                pipelineDetails.put("authors",authorInfoList);
            }
            if (doc.isSetVersion()) {
                pipelineDetails.put("version",doc.getVersion());
            }
        }

        // Step ids and descriptions
        PipelineData.Steps.Step[] steps = pipelineData.getSteps().getStepArray();
        List<Map<String,String>> stepInfoList = new ArrayList<Map<String,String>>();
        for (PipelineData.Steps.Step aStep : steps) {
            Map<String,String> stepInfo = new HashMap<String,String>();
            stepInfo.put("id",aStep.getId());
            stepInfo.put("description",aStep.getDescription());
            stepInfoList.add(stepInfo);
        }
        pipelineDetails.put("steps",stepInfoList);

        // Project-level param defaults
        List<ArcPipelineparameterdataI> params = projectPipelineData.getParameters_parameter();
        List<Map<String,Object>> paramInfoList = new ArrayList<Map<String,Object>>();
        for (ArcPipelineparameterdataI aParamI : params) {
            ArcPipelineparameterdata aParam = (ArcPipelineparameterdata) aParamI;

            Map<String,Object> paramInfo = new HashMap<String,Object>();
            paramInfo.put("name",aParam.getName());
            paramInfo.put("description",aParam.getDescription());

            Map<String,String> paramValues = new HashMap<String,String>();
            String csv = aParam.getCsvvalues();
            String schemaLink = aParam.getSchemalink();
            if (StringUtils.isNotBlank(schemaLink) || StringUtils.isNotBlank(csv)) {
                if (StringUtils.isNotBlank(schemaLink)) {
                    paramValues.put("schemaLink",schemaLink);
                }else {
                    paramValues.put("csv",csv);
                }
                paramInfo.put("values",paramValues);
            } else {
                throw new Exception("Values not set for parameter " + aParam.getName());
            }
            paramInfoList.add(paramInfo);
        }
        pipelineDetails.put("inputParameters",paramInfoList);

        return pipelineDetails;
    }
}
