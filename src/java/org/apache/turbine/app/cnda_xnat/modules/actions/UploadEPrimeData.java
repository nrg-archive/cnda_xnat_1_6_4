//Copyright 2007 Washington University School of Medicine All Rights Reserved
/*
 * Created on Oct 31, 2007
 *
 */
package org.apache.turbine.app.cnda_xnat.modules.actions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.PipelineFileUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParametersDocument;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.apache.commons.fileupload.FileItem;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ParameterParser;
import org.apache.velocity.context.Context;
import org.apache.xmlbeans.XmlOptions;
import org.nrg.xdat.turbine.modules.actions.SecureAction;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFT;
import org.nrg.xft.XFTItem;
import org.nrg.xft.security.UserI;

public class UploadEPrimeData extends SecureAction {
    @Override
	public void doPerform(RunData data, Context context) {
    	try {
	    	ParameterParser paramParser = data.getParameters();
	    	
		// personality/IPIP is included
	    	if (paramParser.getBoolean("pIncluded")) {
	    		runPipeline(paramParser, data, context, "Ipip");
	    	}
	    	
		// neuropsych/compbat is included
	    	if (paramParser.getBoolean("npIncluded")) {
	    		runPipeline(paramParser, data, context, "CompBat");
	    	}
	    	
	    	XFTItem subject = (XFTItem)(TurbineUtils.GetItemBySearch(data,true));
	    	TurbineUtils.SetSearchProperties(data, subject);
	    	this.redirectToReportScreen(subject, data);
    	} catch (Exception e) {
	    	error(e,data);
 	   }
	}
    
    private void runPipeline(ParameterParser paramParser, RunData data, Context context, String type) throws Exception {
		String fileId = "";
		
		if (type == "Ipip") fileId = "pFile";
		else if (type == "CompBat") fileId = "nPfile";
    	
    	FileItem fi = getZipFile(paramParser, fileId);
    	
    	if (fi != null) {
    		// set up build directory
    		String buildDir = getBuildDirectory(paramParser);
    		
    		// write zip file into build directory
            String zipFilename = writeZipFile(fi, buildDir);
            
    		// build parameter list
            Parameters parameters = getParameters(paramParser, zipFilename, type);
	        String paramFileDir = getParamFileDirectory(paramParser, buildDir);
	        String paramFile = writeParamFile(paramFileDir, parameters, type);
			
	        // launch pipeline
	    	launchPipeline(paramParser, new XnatPipelineLauncher(data, context), TurbineUtils.getUser(data), paramFile, buildDir, type);
    	}
    }
    
    // Helper functions
    private FileItem getZipFile(ParameterParser paramParser, String name) {
    	return paramParser.getFileItem(name);
    }
    
    private String getBuildDirectory(ParameterParser paramParser) {
    	String buildDir = PipelineFileUtils.getBuildDir(paramParser.getString("project"), true) + new SimpleDateFormat("yyMMdd_HHmmss").format(new Date());
    	
    	File buildDirFile = new File(buildDir);
        if (!buildDirFile.exists()) buildDirFile.mkdirs();
        
        return buildDir;
    }
    
    private String writeZipFile(FileItem fi, String buildDir) throws Exception {
    	 String filename = fi.getName();
         
         // IE's input file control will send the full path, chop it off to only the filename since we're moving it
     	int index = filename.lastIndexOf("\\");
         
     	if (index < filename.lastIndexOf("/"))
         	index = filename.lastIndexOf("/");
     	
     	if (index > 0)
     		filename = filename.substring(index + 1);
     	
        File uploaded = new File(buildDir + File.separator + filename);
        fi.write(uploaded);
     	
     	return filename;
    }
    
    private Parameters getParameters(ParameterParser paramParser, String zipFilename, String type) {
    	Parameters parameters = Parameters.Factory.newInstance();
    	
    	ParameterData param = parameters.addNewParameter();
     	param.setName("visit_id");
     	param.addNewValues().setUnique(paramParser.getString("visit"));
    	
     	param = parameters.addNewParameter();
     	param.setName("sessionLabel");
     	param.addNewValues().setUnique(paramParser.getString("label"));
     	
     	param = parameters.addNewParameter();
     	param.setName("subject_ID");
     	param.addNewValues().setUnique(paramParser.getString("subject_ID"));
     	
     	param = parameters.addNewParameter();
     	param.setName("xnat_project");
     	param.addNewValues().setUnique(paramParser.getString("project"));
     	
     	if(type == "Ipip") {
	     	param = parameters.addNewParameter();
	     	param.setName("ptNotes");
	     	param.addNewValues().setUnique(paramParser.getString("ptNotes"));
	     	
	     	param = parameters.addNewParameter();
	     	param.setName("csNotes");
	     	param.addNewValues().setUnique(paramParser.getString("csNotes"));
     	}
     	else if(type == "CompBat") {
     		param = parameters.addNewParameter();
         	param.setName("note");
         	param.addNewValues().setUnique(paramParser.getString("npNotes"));
     	}
     	
     	param = parameters.addNewParameter();
     	param.setName("zipfile");
     	param.addNewValues().setUnique(zipFilename);

     	param = parameters.addNewParameter();
     	param.setName("catalogPath");
     	param.addNewValues().setUnique(XFT.GetPipelinePath() + "/catalog");

     	param = parameters.addNewParameter();
     	param.setName("notify");
     	param.addNewValues().setUnique(paramParser.getBoolean("notify") ? "1" : "0");

     	param = parameters.addNewParameter();
     	param.setName("mailhost");
     	param.addNewValues().setUnique(AdminUtils.getMailServer());
     	
     	return parameters;
    }
    
    private String getParamFileDirectory(ParameterParser paramParser, String buildDir) {
		String paramFileDir = buildDir + File.separator + paramParser.getString("label");
		
		File dir = new File(paramFileDir);
        if (!dir.exists()) dir.mkdirs();
        
        return paramFileDir;
    }
    
    private String writeParamFile(String paramFileDir, Parameters parameters, String type) throws IOException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        
        String paramFileName = "ePrime" + type + "_params_" + formatter.format(new Date()) + ".xml";
        
        File paramFile = new File(paramFileDir + File.separator + paramFileName);
        
        ParametersDocument paramDoc = ParametersDocument.Factory.newInstance();
        paramDoc.addNewParameters().set(parameters);
        
        paramDoc.save(paramFile,new XmlOptions().setSavePrettyPrint().setSaveAggressiveNamespaces());
        
        return paramFile.getAbsolutePath();
    }
    
    private void launchPipeline(ParameterParser paramParser, XnatPipelineLauncher xpl, UserI user, String paramFile, String buildDir, String type) {
    	xpl.setExternalId(paramParser.getString("project"));
    	xpl.setPipelineName("ePrime/ePrime" + type + "Parser.xml");
    	xpl.setParameterFile(paramFile);
    	
    	xpl.setDataType("xnat:subjectData");
    	xpl.setId(paramParser.getString("subject_ID"));
    	xpl.setLabel(paramParser.getString("label"));
    	xpl.setBuildDir(buildDir);
    			    	
    	xpl.setParameter("useremail", user.getEmail());
    	xpl.setParameter("userfullname", XnatPipelineLauncher.getUserName(user));
    	xpl.setParameter("adminemail", AdminUtils.getAdminEmailId());
    	
	// prevent duplicate e-mails
    	xpl.setSupressNotification(true);

	// use alias host to accomodate network configuration on production servers
	xpl.useAlias(true);

    	xpl.launch(null);
    }
}
