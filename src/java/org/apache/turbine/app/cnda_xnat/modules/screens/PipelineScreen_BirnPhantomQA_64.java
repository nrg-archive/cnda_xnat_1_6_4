/*
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 *
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.apache.turbine.app.cnda_xnat.modules.screens;

import java.io.File;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.launchers.BirnPhantomQA;
import org.nrg.pipeline.launchers.GenericDTIProcessingLauncher;
import org.nrg.xdat.om.ArcPipelineparameterdata;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xnat.turbine.modules.screens.DefaultPipelineScreen;

public class PipelineScreen_BirnPhantomQA_64 extends DefaultPipelineScreen{
	String boldScanType;

	static Logger logger = Logger.getLogger(PipelineScreen_BirnPhantomQA.class);


	 public void finalProcessing(RunData data, Context context){
		 try {
			XnatMrsessiondata mr = (XnatMrsessiondata) om;
	    	context.put("mr", mr);
	    	LinkedHashMap<String,String> buildableScanTypes = new LinkedHashMap<String,String>();

	    	String selfStatus = WrkWorkflowdata.GetLatestWorkFlowStatusByPipeline(mr.getId(), XnatMrsessiondata.SCHEMA_ELEMENT_NAME, BirnPhantomQA.LOCATION+File.separator +BirnPhantomQA.NAME_64, mr.getProject(), TurbineUtils.getUser(data));
	    	if (selfStatus.equalsIgnoreCase(WrkWorkflowdata.COMPLETE)) {
	    		data.setMessage("This pipeline has already completed. Relaunching the pipeline may result in loss of processed files");
	    	}
	    	   ArcPipelineparameterdata  boldParam = 	getProjectPipelineSetting(BirnPhantomQA.BOLD_PARAM);
			    if (boldParam != null) {
			      boldScanType = boldParam.getCsvvalues();
			    }
				if (boldScanType != null)
	        		buildableScanTypes.put(BirnPhantomQA.BOLD, boldScanType);
				context.put("buildableScanTypes", buildableScanTypes);
		 }catch(Exception e) {
			 logger.error("Possibly the project wide pipeline has not been set", e);
			 e.printStackTrace();
		 }
	 }


}
