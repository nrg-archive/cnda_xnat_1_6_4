
package org.apache.turbine.app.cnda_xnat.modules.screens;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.nrg.xdat.model.ArcPipelinedataI;
import org.nrg.xdat.om.ArcProject;
import org.nrg.xdat.om.ArcProjectPipeline;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.ArcPipelineparameterdata;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatSubjectassessordata;
// import org.nrg.xdat.model.XnatSubjectassessordataI;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xnat.turbine.modules.screens.DefaultPipelineScreen;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xft.ItemI;

public class PipelineScreen_RegisterMRToAtlas extends DefaultPipelineScreen  {

        public static final String MR = "MR";
        public static final String SCANTYPE_PARAM = "scantype";

        public static final String MRXSITYPE = "xnat:mrSessionData";
        public static final String PETXSITYPE = "xnat:petSessionData";

        String mrScanType;
        static Logger logger = Logger.getLogger(PipelineScreen_RegisterMRToAtlas.class);

        public void finalProcessing(RunData data, Context context){

            try {
                String projectId = ((String)TurbineUtils.GetPassedParameter("project",data));
                String pipelinePath = ((String)TurbineUtils.GetPassedParameter("pipeline",data));
                String schemaType = ((String)TurbineUtils.GetPassedParameter("schema_type",data));
                ArcProject arcProject = ArcSpecManager.GetFreshInstance().getProjectArc(projectId);

                if (schemaType.equals(XnatProjectdata.SCHEMA_ELEMENT_NAME)) {
                    ArcProjectPipeline pipelineData = (ArcProjectPipeline)arcProject.getPipelineByPath(pipelinePath);
                    context.put("pipeline", pipelineData);
                    setParameters(pipelineData, context);
                }else {
                    ArcPipelinedataI pipelineData = arcProject.getPipelineForDescendantByPath(schemaType, pipelinePath);
                    context.put("pipeline", pipelineData);
                    setParameters(pipelineData, context);
                }

                context.put("mrKey", MR);

                ArcPipelineparameterdata scanTypeParam = getProjectPipelineSetting(SCANTYPE_PARAM);
                if (scanTypeParam != null) {
                    mrScanType = scanTypeParam.getCsvvalues();
                }
                context.put("mrScanType",mrScanType);

                XnatImagesessiondata imageSession = null;
                ArrayList mrScans = null;
                if (schemaType.equalsIgnoreCase(MRXSITYPE)) {
                    imageSession = (XnatMrsessiondata) om;
                    setHasDicomFiles(imageSession, mrScanType, context);
                    mrScans = ((XnatMrsessiondata) om).getUnionOfScansByType(mrScanType);
                } else if (schemaType.equalsIgnoreCase(PETXSITYPE)) {
                    imageSession = (XnatPetsessiondata) om;
                    setHasDicomFiles(imageSession, mrScanType, context);
                    mrScans = ((XnatPetsessiondata) om).getUnionOfScansByType(mrScanType);
                }
                context.put("imageSession", imageSession);
                context.put("mrScans",mrScans);

                String selfStatus = WrkWorkflowdata.GetLatestWorkFlowStatusByPipeline(imageSession.getId(), schemaType, pipelinePath, imageSession.getProject(), TurbineUtils.getUser(data));
                if (selfStatus.equalsIgnoreCase(WrkWorkflowdata.COMPLETE)) {
                    data.setMessage("This pipeline has already completed. Relaunching the pipeline may result in loss of processed files");
                }

                context.put("projectSettings", projectParameters);

                // LinkedHashMap<String,String> buildableScanTypes = new LinkedHashMap<String,String>();
                // if (mrScanType != null) {
                //     buildableScanTypes.put(MR, mrScanType);
                // }

                // context.put("buildableScanTypes", buildableScanTypes);

            }catch(Exception e) {
                logger.error("Possibly the project wide pipeline has not been set", e);
                e.printStackTrace();
            }
        }
    }
