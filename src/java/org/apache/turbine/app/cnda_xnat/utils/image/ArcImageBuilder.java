//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * ArcBuilder.java
 *
 * Created on May 17, 2002, 9:17 AM
 */

package org.apache.turbine.app.cnda_xnat.utils.image;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
/**
 *
 * @author  dan 
 * @version
 */
public class ArcImageBuilder {
	static Logger logger = Logger.getLogger(ArcImageBuilder.class);
    
    String buildCommand = ArcProperties.getToolPath() + "arc-build";
    
    boolean buildMprage = false;
    boolean buildFlash = false;
    boolean buildDti = false;
    boolean buildTse = false;
    
    boolean buildFlair = false;
    boolean buildFlash3d = false;
	boolean buildMef5 = false;
	boolean buildMef30 = false;
	boolean buildMtc5 = false;
	//boolean buildGlk_mprage = false;
    
    boolean buildNow = false;
    boolean useEmail = false;
      
    List mprageScans;
    List flashScans;
    List dtiScans;
    List tseScans;
    List mef5Scans;
    List mef30Scans;
    List mtc5Scans;
	List flairScans;
   // List flash3dScans;
   // List lo_resScans;
    
    List email;
    String id;
    String atlas;
    String preArc;
    
    /** Creates new ArcBuilder */
    public ArcImageBuilder(String id, String preArc) {
        this.id = id;
        this.preArc = preArc;
    }
        
    public void setEmail(List email){
        useEmail = true;
        this.email = email;
    }
    
    public void setMprage(List scans){
        buildMprage = true;
        mprageScans = scans;
    }
    
    public void setFlash(List scans){
        buildFlash = true;
        flashScans = scans;
    }
    
    public void setDti(List scans){
        buildDti = true;
        dtiScans = scans;
    }
    
    public void setTse(List scans){
        buildTse = true;
        tseScans = scans;
    }

	public void setMef5(List scans){
		buildMef5 = true;
		mef5Scans = scans;
	}

	public void setMef30(List scans){
		buildMef30 = true;
		mef30Scans = scans;
	}

	public void setMtc5(List scans){
		buildMtc5 = true;
		mtc5Scans = scans;
	}
    
	public void setFlair(List scans){
		buildFlair = true;
		flairScans = scans;
	}

/*	public void setFlash3d(List scans){
		buildFlash3d = true;
		flash3dScans = scans;
	} */

    public void setBuildNow(boolean now){
        buildNow = now;
    }
    
    public void doBuild(String username, String password) throws IOException{
        
        String args = "";
        String queueCommand = "";
        Iterator it;
        
        args += " -s " + id;
        
        if (useEmail){
            it = email.iterator();
            while (it.hasNext()){
                args += " -e " + (String)it.next();
            }
        }
        
        if (buildMprage){
            it = mprageScans.iterator();
            while (it.hasNext()){
                args += " -m " + (String)it.next();
            }
        }
        
        if (buildFlash){
            it = flashScans.iterator();
            while (it.hasNext()){
                args += " -f " + (String)it.next();
            }
        }
        
        if (buildDti){
            it = dtiScans.iterator();
            while (it.hasNext()){
                args += " -d " + (String)it.next();
            }
        }
        
        if (buildTse){
            it = tseScans.iterator();
            while (it.hasNext()){
                args += " -t " + (String)it.next();
            }
        }
		if (buildMef5){
			it = mef5Scans.iterator();
			while (it.hasNext()){
				args += " -mef5 " + (String)it.next();
			}
		}

		if (buildMef30){
			it = mef30Scans.iterator();
			while (it.hasNext()){
				args += " -mef30 " + (String)it.next();
			}
		}

		if (buildMtc5){
			it = mtc5Scans.iterator();
			while (it.hasNext()){
				args += " -mtc5 " + (String)it.next();
			}
		}

		if (buildFlair){
			it = flairScans.iterator();
			while (it.hasNext()){
				args += " -flair " + (String)it.next();
			}
		} 

		args += " -u " +  username;
		args += " -p " +  password;

		args += " -atlas " + atlas;
		        
        if (!buildNow){
            queueCommand = ArcProperties.getToolPath() + "arc-qadd ";
        }
         
        //System.out.println("Building images with command: " + queueCommand + buildCommand + args);
        logger.info("Building images with command: " + queueCommand + buildCommand + args);
        System.out.println("Building images with command: " + queueCommand + buildCommand + args);
        Runtime.getRuntime().exec(queueCommand + buildCommand + args);
        
    }
    
    public static void main(String [] args){
        
        ArcImageBuilder builder = new ArcImageBuilder("subject", ArcProperties.getPreArcPath() );
        ArrayList _mprageScans = new ArrayList();
        _mprageScans.add("1-1"); _mprageScans.add("1-2"); _mprageScans.add("1-3");
        ArrayList _flashScans = new ArrayList();
        _flashScans.add("1-4"); _flashScans.add("1-5"); _flashScans.add("1-6");
        ArrayList _dtiScans = new ArrayList();
        _dtiScans.add("1-7"); _dtiScans.add("1-8"); _dtiScans.add("1-9");
        ArrayList _tseScans = new ArrayList();
        _tseScans.add("1-10"); _tseScans.add("1-11"); _tseScans.add("1-12");
        ArrayList mail = new ArrayList();
        mail.add("dan@iac.wustl.edu");
        builder.setEmail(mail);
        builder.setMprage(_mprageScans);
        builder.setFlash(_flashScans);
        builder.setDti(_dtiScans);
        builder.setTse(_tseScans);
        try {
            builder.doBuild("test","test");
        } catch (Exception e) { e.printStackTrace(); }
    }
    
    
	/**
	 * @param string
	 */
	public void setAtlas(String string) {
		atlas = string;
	}

}
