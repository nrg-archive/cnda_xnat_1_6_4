<?xml version="1.0" encoding="UTF-8"?>

<xs:schema targetNamespace="http://nrg.wustl.edu/condr" xmlns:xdat="http://nrg.wustl.edu/xdat" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xnat="http://nrg.wustl.edu/xnat" xmlns:sf="http://nrg.wustl.edu/sf" xmlns:tissue="http://nrg.wustl.edu/tissue" xmlns:condr="http://nrg.wustl.edu/condr" xmlns:xnat_a="http://nrg.wustl.edu/xnat_assessments" elementFormDefault="qualified" attributeFormDefault="unqualified">
	<xs:import namespace="http://nrg.wustl.edu/xnat" schemaLocation="../xnat/xnat.xsd"/>
	<xs:import namespace="http://nrg.wustl.edu/sf" schemaLocation="../subjforms/subjforms.xsd"/>
	<xs:import namespace="http://nrg.wustl.edu/xnat_assessments" schemaLocation="../assessments/assessments.xsd"/>
	<xs:import namespace="http://nrg.wustl.edu/tissue" schemaLocation="../tissue/tissue.xsd"/>
	<xs:element name="CondrMedicalHistory" type="condr:condrMedicalHistory" />
	<xs:element name="ClinicalEncounter" type="condr:clinicalEncounter" />
		<xs:element name="BrainColl" type="condr:brainCollData">
		<xs:annotation>
			<xs:documentation>Brain Collection Session</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:element name="Brain" type="condr:brainData">
		<xs:annotation>
			<xs:documentation>Brain Tissue</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:element name="BrainPathology" type="condr:brainPathData">
		<xs:annotation>
			<xs:documentation>Brain Pathology</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:complexType name="condrMedicalHistory">
		<xs:complexContent>
			<xs:extension base="sf:medicalHistory">
				<xs:sequence>
					<xs:element name="tumorPredisposition" type="xs:string" minOccurs="0" />
					<xs:element name="familyTumorSyndrome" type="xs:boolean" minOccurs="0" />
					<xs:element name="familyTumorSpecificType" type="xs:boolean" minOccurs="0" />
					<xs:element name="familyTumorNotes" minOccurs="0">
						<xs:simpleType>
							<xs:restriction base="xs:string">
								<xs:maxLength value="1000" />
							</xs:restriction>
						</xs:simpleType>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="clinicalEncounter">
		<xs:complexContent>
			<xs:extension base="xnat:subjectAssessorData">
				<xs:sequence>
					<xs:element name="karnofskyPerformanceStatus" minOccurs="0">
						<xs:simpleType>
							<xs:restriction base="xs:integer">
								<xs:minInclusive value="0" />
								<xs:maxInclusive value="100" />
							</xs:restriction>
						</xs:simpleType>
					</xs:element>
					<xs:element name="rpaClass" type="xs:string" minOccurs="0" />
					<xs:element name="newDeficits" type="sf:deficits" minOccurs="0" />
					<xs:element name="clinicalDeterioration" type="xs:boolean" minOccurs="0" />
					<xs:element name="steroids" type="xs:boolean" minOccurs="0" />
					<xs:element name="steroidDose" type="xs:string" minOccurs="0" />
					<xs:element name="steroidDoseIncreasing" type="xs:boolean" minOccurs="0" />
					<xs:element name="progression" type="xs:boolean" minOccurs="0" />
					<xs:element name="pseudoProgression" type="xs:boolean" minOccurs="0" />
					<xs:element name="response" type="xs:string" minOccurs="0" />
					<xs:element name="changeInTreatment" type="xs:boolean" minOccurs="0" />
					<xs:element name="changeInTreatmentTo" type="xs:string" minOccurs="0" />
					<xs:element name="clinicalEncounterNotes" minOccurs="0">
						<xs:simpleType>
							<xs:restriction base="xs:string">
								<xs:maxLength value="1000" />
							</xs:restriction>
						</xs:simpleType>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	
	
	<xs:complexType name="brainData">	
		<xs:annotation>
			<xs:documentation>Brain Specimen</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
		<xs:extension base="tissue:specData">
		<xs:sequence>
			<xs:element name="descLoc" minOccurs="0" type="xs:string">
				<xs:annotation>
					<xs:documentation>Descriptive Location</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="coordX" minOccurs="0" type="xs:float">
				<xs:annotation>
					<xs:documentation>Coordinate X</xs:documentation>
				</xs:annotation>
			</xs:element>	
			<xs:element name="coordY" minOccurs="0" type="xs:float">
				<xs:annotation>
					<xs:documentation>Coordinate Y</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="coordZ" minOccurs="0" type="xs:float">
				<xs:annotation>
					<xs:documentation>Coordinate Z</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="coordSource" minOccurs="0"
				type="xs:string">
				<xs:annotation>
					<xs:documentation>
						Coordinate Source
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="sampSplit" minOccurs="0" type="xs:boolean">
				<xs:annotation>
					<xs:documentation>Sample split</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="surgPath" minOccurs="0" type="xs:boolean">
				<xs:annotation>
					<xs:documentation>Surgical path</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="tumorBank" minOccurs="0" type="xs:boolean">
				<xs:annotation>
					<xs:documentation>Tumor bank</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="surgeryTimeNotReported" minOccurs="0" type="xs:boolean">
				<xs:annotation>
					<xs:documentation>Flags not reported for surgery time.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="coordColl" minOccurs="0" type="xs:boolean">
				<xs:annotation>
					<xs:documentation>Coordinates collected</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="screenCapColl" minOccurs="0" type="xs:boolean">
				<xs:annotation>
					<xs:documentation>Screen captures collected</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="rgraphApp" minOccurs="0" type="xs:string">
				<xs:annotation>
					<xs:documentation>Radiographic appearance</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="rgraphAppNote" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
						Radiographic appearance comments
					</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:maxLength value="1000"></xs:maxLength>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<!-- <xs:element name="perlLevelQual" minOccurs="0" type="xs:boolean">
				<xs:annotation>
					<xs:documentation>Qualitative Perfusion Level</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="perlLevelQuan" minOccurs="0" type="xs:string">
				<xs:annotation>
					<xs:documentation>Quantitative Perfusion Level</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="oefLevelQual" minOccurs="0" type="xs:boolean">
				<xs:annotation>
					<xs:documentation>Qualitative OEF Level</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="oefLevelQuan" minOccurs="0" type="xs:string">
				<xs:annotation>
					<xs:documentation>Quantitative OEF Level</xs:documentation>
				</xs:annotation>
			</xs:element>	
			<xs:element name="adcLevelQual" minOccurs="0" type="xs:boolean">
				<xs:annotation>
					<xs:documentation>Qualitative ADC Level</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="adcLevelQuan" minOccurs="0" type="xs:int">
				<xs:annotation>
					<xs:documentation>Quantitative ADC Level</xs:documentation>
				</xs:annotation>
			</xs:element>-->
		</xs:sequence>
		</xs:extension>
	</xs:complexContent>	
	</xs:complexType>
	
	<xs:complexType name="brainCollData">	
		<xs:annotation>
			<xs:documentation>Tumor Collection</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
		<xs:extension base="tissue:tissCollData">
		<xs:sequence>
			<xs:element name="tumorLocs" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Tumor Location</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence minOccurs="0">
						<xs:element name="tumorLoc" type="xs:string"
							minOccurs="0" maxOccurs="unbounded">
							<xs:annotation>
								<xs:appinfo>
									<xdat:field
										uniqueComposite="TUMOR_LOC_VALUE">
										<xdat:relation
											uniqueComposite="TUMOR_LOC_VALUE" />
									</xdat:field>
								</xs:appinfo>
							</xs:annotation>
						</xs:element>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="tumorHemisphere" minOccurs="0"
				type="xs:string">
				<xs:annotation>
					<xs:documentation>
						Tumor Hemisphere
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="surgicalExts" minOccurs="0" >
				<xs:annotation>
					<xs:documentation>Surgical Extent</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence minOccurs="0">
						<xs:element name="surgicalExt" type="xs:string"
							minOccurs="0" maxOccurs="unbounded">
							<xs:annotation>
								<xs:appinfo>
									<xdat:field
										uniqueComposite="SURGICAL_EXT_VALUE">
										<xdat:relation
											uniqueComposite="SURGICAL_EXT_VALUE" />
									</xdat:field>
								</xs:appinfo>
							</xs:annotation>
						</xs:element>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="gliadelUsed" minOccurs="0"
				type="xs:boolean">
				<xs:annotation>
					<xs:documentation>Gliadel Used</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="surgeons" minOccurs="0" >
				<xs:annotation>
					<xs:documentation>Surgeons</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence minOccurs="0">
						<xs:element name="surgeon" type="xs:string" minOccurs="0" maxOccurs="unbounded">
							<xs:annotation>
								<xs:appinfo>
									<xdat:field
										uniqueComposite="SURGEON_VALUE">
										<xdat:relation
											uniqueComposite="SURGEON_VALUE" />
									</xdat:field>
								</xs:appinfo>
							</xs:annotation>
						</xs:element>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="bloodColl" minOccurs="0"
				type="xs:boolean">
				<xs:annotation>
					<xs:documentation>Blood Collected</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="redTop" minOccurs="0" type="xs:boolean">
				<xs:annotation>
					<xs:documentation>
						Red Top Blood Vial Sent
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="purpleTop" minOccurs="0"
				type="xs:boolean">
				<xs:annotation>
					<xs:documentation>
						Purple Top Blood Vial Sent
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="sentToLab" minOccurs="0"
				type="xs:boolean">
				<xs:annotation>
					<xs:documentation>
						Were samples sent to lab?
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="sentToLabDetails" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Details on why samples were not sent to the lab.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:maxLength value="1000"></xs:maxLength>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="complications" minOccurs="0"
				type="xs:boolean">
				<xs:annotation>
					<xs:documentation>Were there complications?</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="complicationsDetails" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Details on any complications.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:maxLength value="1000"></xs:maxLength>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
		</xs:extension>
	 </xs:complexContent>
	</xs:complexType>			
	<xs:complexType name="brainPathData">	
		<xs:annotation>
			<xs:documentation>Brain Pathology</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
		<xs:extension base="tissue:labResultData">
		<xs:sequence>
			<xs:element name="tumorType" minOccurs="0" type="xs:string">
				<xs:annotation>
					<xs:documentation>
						Tumor Type (Primary, Metastatic)
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="primaryTumorType" minOccurs="0" type="xs:string">
				<xs:annotation>
					<xs:documentation>
						Primary Tumor Type (Astocytoma, Meningioma,	etc.)
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="primaryWHOGrade" minOccurs="0" type="xs:string">
				<xs:annotation>
					<xs:documentation>
						Primary WHO Grade (I, II, III (anaplastic), IV
						(glioblastoma))
					</xs:documentation>
				</xs:annotation>
			</xs:element> 
			<xs:element name="metastatOrig" minOccurs="0" type="xs:string">
				<xs:annotation>
					<xs:documentation>
						MetastaticOrigin (breast, lung, colon, etc.)
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="onep19qDeletions" minOccurs="0" type="xs:string">
				<xs:annotation>
					<xs:documentation>
						1p19q Deletions (Present Absent)
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="mgmtPromoterStatus" minOccurs="0" type="xs:string">
				<xs:annotation>
					<xs:documentation>
						MGMT Promoter Status (Methylated, Unmethylated)
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="mib1Index" minOccurs="0" type="xs:float">
				<xs:annotation>
					<xs:documentation>
						MIB 1 Index (float %)
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="mib1IndexNotReported" minOccurs="0" type="xs:boolean">
				<xs:annotation>
					<xs:documentation>
						Flags if the MIB 1 Index was reported or not 
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="pten" minOccurs="0" type="xs:string">
				<xs:annotation>
					<xs:documentation>
						PTEN (Wild Type, Deleted/Mutated)
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="p53" minOccurs="0" type="xs:string">
				<xs:annotation>
					<xs:documentation>
						P53 (Wild Type, deleted/mutated)
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="othFeat" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Other Features</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:maxLength value="25000" />
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
		</xs:extension>
		</xs:complexContent>
	</xs:complexType>
</xs:schema>
